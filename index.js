const express = require('express')
const app = express()
const port = 3000
// const db = require("./appserv/DBinterfaces");
const students = require("./appserv/students");
const admins = require("./appserv/admins");
const subjects = require('./appserv/subjects');
const rooms = require("./appserv/rooms");
const classes = require("./appserv/classes");
const classLists = require("./appserv/classList");
// const shifts = require("./appserv/shifts");
const users = require("./appserv/users");
// const examInfos = require("./appserv/examInfos");
// const examLists = require("./appserv/examLists");


//For passport
const sessionLong = 30;
const session = require('express-session');
const Passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static('FrontEnd'))

//For passport
app.use(session({
    secret: "mysecret",
    cookie: {
        maxAge:1000*60*sessionLong
    }
}))
app.use(Passport.initialize());
app.use(Passport.session());

app.get('/api/*', function (req, res) {
    if (req.isAuthenticated()) {
        switch(req.path){
            case "/api/get-exam-info-detail":
                if(req.query.examInfoID){
                    require("./appserv/examInfos").showSpecificExamInfo(req.query.examInfoID,result=>{
                        if(result){
                            rooms.showSpecificRoom(result.roomID,roomInfo=>{
                                result.roomInfo = roomInfo;
                                require("./appserv/examInfos").listExamInfosWithCondition(`ExamInfoID='${result.examInfoID}'`,student_list=>{
                                    result.used = student_list.length;
                                    subjects.showSpecificSubject(result.subjectID, subjectInfo=>{
                                        result.subjectInfo = subjectInfo;
                                        require('./appserv/shifts').showSpecificShift(result.shiftID,shiftInfo=>{
                                            result.shiftInfo = shiftInfo;
                                            res.json(result);
                                        })
                                    });
                                })
                            })
                        }
                        else{
                            res.status(404).send("No exam info match");
                        }
                    })
                }
                break;
            case "/api/list-subjects":
                if(req.user.roleType == "student"){
                    subjects.listSubjectsRegisteredByStudentID(req.user.userID,(result)=>{
                        if(result){
                            res.json(result);
                        }
                        else{
                            res.status(400).send("No information");
                        }
                    })
                }
                else{
                    if(req.query.studentID){
                        subjects.listSubjectsRegisteredByStudentID(req.query.studentID,(result)=>{
                            if(result){
                                res.json(result);
                            }
                            else{
                                res.status(400).send("Student not found");
                            }
                        })
                    }
                    else{
                        subjects.listSubjects((result)=>{
                            if(result){
                                result.forEach((element,i,arr) => {
                                    students.listStudentsJoinSubjectWithSubjectID(element, (studentNum)=>{
                                        arr[i]['studentNum'] = studentNum.length;
                                        if(i == arr.length -1){
                                            res.json(arr);
                                        }
                                    })
                                });
                            }
                            else{
                                res.sendStatus(500);
                            }
                        });
                    }
                }
                break;
            case "/api/get-student-info":
                if(req.user.roleType=="student"){
                    students.showSpecificStudent(req.user.userID,(result)=>{
                        if(result){
                            res.json(result);
                        }
                        else{
                            res.status(404).send("No information");
                        }
                    })
                }
                else if(req.user.roleType == "admin"){
                    students.showSpecificStudent(req.query.id,(result)=>{
                        if(result){
                            res.json(result);
                        }
                        else{
                            res.status(404).send("No student match id " + req.query.id);
                        }
                    }) 
                }
                else{
                    res.sendStatus(401);
                }
                break;
            case "/api/whoami":
                users.showSpecificUser(req.user.userID, result=>{
                    if(result.roleType=="student"){
                        students.showSpecificStudent(req.user.userID, studentInfo=>{
                            res.json({
                                role: req.user.roleType,
                                ID: req.user.userID,
                                info: studentInfo
                            })
                        });
                    }
                    else if(result.roleType == "admin"){
                        admins.showSpecificAdmin(req.user.userID, adminInfo=>{
                            res.json({
                                role: req.user.roleType,
                                ID: req.user.userID,
                                info: adminInfo
                            });
                        })
                    }
                    else if(result.roleType == "root"){
                        res.json({
                            role: req.user.roleType,
                            ID: req.user.userID,
                            info: null
                        });
                    }
                    else{
                        res.sendStatus(401);
                    }
                })
                break;
            case "/api/list-exam-subject":
                subjects.listSubjectsRegisteredByStudentID(req.user.userID,result=>{
                    res.json(result);
                })
                break;
            case "/api/list-exam-info-by-subject":
                require("./appserv/examInfos").listExamInfosWithCondition(`subjectID='${req.query.subjectID}'`,result=>{
                    if(result.length!=0){
                        result.forEach((ele,i,arr)=>{
                            rooms.showSpecificRoom(ele.roomID,roomInfo=>{
                                arr[i].roomInfo = roomInfo;
                                require("./appserv/examInfos").listExamInfosWithCondition(`ExamInfoID='${ele.examInfoID}'`,student_list=>{
                                    arr[i].used = student_list.length;
                                    subjects.showSpecificSubject(ele.subjectID, subjectInfo=>{
                                        arr[i].subjectInfo = subjectInfo;
                                        require('./appserv/shifts').showSpecificShift(ele.shiftID,shiftInfo=>{
                                            arr[i].shiftInfo = shiftInfo;
                                            if(i == arr.length - 1){
                                                res.json(arr);
                                            }
                                        })
                                    });
                                })
                            })
                        })
                    }
                    else{
                        res.json(result);
                    }
                });
                break;
            case "/api/get-registered":
                if(req.query.studentID) {
                    require("./appserv/examInfos").listExamInfosRegisteredByStudentID(req.query.studentID, (resultExamInfoList) => {
                        res.json(resultExamInfoList);
                    })
                } else {
                    res.status(400).send("Wrong format");
                }
                break;
            case "/api/dereg":
                if(req.user.roleType == "student"){
                    if(req.query.examInfoID){
                        require("./appserv/examLists").deleteExamList(req.query.examInfoID,req.user.userID,result=>{
                            if(!result) res.sendStatus(200);
                            else res.sendStatus(418);
                        });
                    }
                    else{
                        res.status(400).send("Missing examInfoID");
                    }
                }
                else{
                    if(req.query.examInfoID && req.query.studentID){
                        require("./appserv/examLists").deleteExamList(req.query.examInfoID,req.query.studentID,result=>{
                            if(!result) res.sendStatus(200);
                            else res.sendStatus(418);
                        });
                    }
                    else{
                        res.status(400).send("Bad request");
                    }
                }
                break;
            default: 
                if(req.user.roleType == "admin"){
                    switch(req.path){
                        case "/api/list-student-in-exam-info":
                            if(req.query.examInfoID){
                                student_in_exam_info = [];
                                require("./appserv/examInfos").showSpecificExamInfo(req.query.examInfoID,examInfo=>{
                                    if(examInfo){
                                        require("./appserv/examLists").listStudentsInExamsWithCondition(`examInfoID='${req.query.examInfoID}'`,student_list=>{
                                            if(student_list.length==0){
                                                res.json([]);
                                            }
                                            else{
                                                student_list.forEach((ele,i,arr)=>{
                                                    students.showSpecificStudent(ele.studentID, studentInfo=>{
                                                        if(studentInfo){
                                                            classLists.getSBDByStudentIDAndSubjectID(studentInfo.studentID,examInfo.subjectID,SBD=>{
                                                                studentInfo.SBD = SBD;
                                                                student_in_exam_info.push(studentInfo);
                                                                if(i == arr.length-1){
                                                                    res.json(student_in_exam_info);
                                                                }
                                                            })
                                                        }
                                                    });
                                                })
                                            }
                                        });
                                    }
                                    else{
                                        res.status(404).send("exam info not found");
                                    }
                                })
                            }
                            else{
                                res.sendStatus(400);
                            }
                            break;
                        case "/api/list-exam-info":
                            require("./appserv/examInfos").listExamInfos(result=>{
                                if(result.length!=0){
                                    result.forEach((ele,i,arr)=>{
                                        rooms.showSpecificRoom(ele.roomID,roomInfo=>{
                                            arr[i].roomInfo = roomInfo;
                                            require("./appserv/examLists").listStudentsInExamsWithCondition(`examInfoID='${ele.examInfoID}'`,student_list=>{
                                                arr[i].used = student_list.length;
                                                subjects.showSpecificSubject(ele.subjectID, subjectInfo=>{
                                                    arr[i].subjectInfo = subjectInfo;
                                                    require('./appserv/shifts').showSpecificShift(ele.shiftID,shiftInfo=>{
                                                        arr[i].shiftInfo = shiftInfo;
                                                        if(i == arr.length - 1){
                                                            res.json(arr);
                                                        }
                                                    })
                                                });
                                            })
                                        })
                                    })
                                }
                                else{
                                    res.json(result);
                                }
                            })
                            break;
                        case "/api/list-students":
                            students.listStudents(result=>{
                                if(result){
                                    res.json(result);
                                }
                                else{
                                    res.status(400).send("No further information");
                                }
                            });
                            break;
                        case "/api/list-rooms":
                            rooms.listRooms(result=>{
                                if(result){
                                    res.json(result);
                                }
                                else{
                                    res.status(400).send("No further information");
                                }
                            });
                            break;
                        case "/api/list-classes":
                            if(req.query.subjectID){
                                classes.listClassesWithSubjectID(req.query.subjectID, (result)=>{
                                    if(result){
                                        res.json(result);
                                    }
                                    else{
                                        res.status(400).send("Subject ID not found");
                                    }
                                });
                            }
                            else{
                                classes.listClasses(result=>{
                                    if(result){
                                        res.json(result);
                                    }
                                    else{
                                        res.status(400).send("No further information");
                                    }
                                });
                            }
                            break;
                        case "/api/list-class-lists":
                            if(req.query.classID){
                                classLists.listStudentsInClassID(req.query.classID, result=>{
                                    if(result){
                                        result.forEach((ele,i,arr)=>{
                                            students.showSpecificStudent(ele.studentID, studentInfo => {
                                                arr[i].studentName = studentInfo.lastName + " " + studentInfo.firstName;
                                                if(i==arr.length-1){
                                                    res.json(arr);// cần thêm thông tin sinh viên
                                                }
                                            })
                                        })
                                    }
                                    else{
                                        res.status(400).send("No further information");
                                    }
                                });
                            }
                            else{
                                res.sendStatus(400);
                            }
                            break;
                        case "/api/list-shifts":
                            if(req.query.roomID && req.query.subjectID){
                                require("./appserv/shifts").listShifts(result=>{
                                    if(result.length!=0){
                                        result.forEach((ele,i,arr)=>{
                                            require("./appserv/examInfos").isExamInfoAvailable({
                                                shiftID: ele.shiftID,
                                                startTime: ele.startTime,
                                                examDay: ele.examDay,
                                                roomID: req.query.roomID,
                                                subjectID: req.query.subjectID
                                            },(result)=>{
                                                arr[i].avail = result;
                                                if(i==arr.length-1){
                                                    res.json(arr);
                                                }
                                            })
                                        });
                                    }
                                    else if(result.length==0){
                                        res.json([]);
                                    }
                                    else {
                                        res.sendStatus(500);
                                    }
                                });
                            }
                            else{
                                require("./appserv/shifts").listShifts(result=>{
                                    if(result){
                                        res.json(result);
                                    }
                                    else{
                                        res.status(400).send("No further information");
                                    }
                                });
                            }
                            break;
                        case "/api/list-exams":
                            require("./appserv/examLists").listStudentsInExams(result=>{
                                if(result){
                                    res.json(result);
                                }
                                else{
                                    res.status(400).send("No further information");
                                }
                            });
                            break;
                        case "/api/list-exam-info":
                            require("./appserv/examInfos").listExamInfo(result=>{
                                if(result){
                                    res.json(result);
                                }
                                else{
                                    res.status(400).send("No further information");
                                }
                            });
                            break;
                        case "/api/get-class-detail":
                            classes.showSpecificClass(req.query.id, (result)=>{
                                if(result){
                                    res.json(result);
                                }
                                else{
                                    res.status(404).send("No class match id " + req.query.id);
                                }
                            })
                            break;
                        case "/api/delete-student":
                            if(req.query.id && !isNaN(parseInt(req.query.id))){    
                                students.deleteStudent(req.query.id,(error,affectedRows)=>{
                                    if(error || affectedRows == 0){
                                        res.status(500).send('Failed');
                                    }
                                    else{
                                        res.status(204).send('Successfully deleted');
                                    }
                                });
                            }
                            else{
                                res.status(400).send("Bad request");
                            }
                            break;
                        case "/api/delete-class":
                            if(req.query.id){
                                classes.deleteClass(req.query.id,result=>{
                                    if(!result){
                                        res.status(204).send('Successfully deleted');
                                    }
                                    else{
                                        res.status(500).send('Failed');
                                    }
                                })
                            }
                            break;
                        case "/api/delete-subject":
                            if(req.query.id){
                                subjects.deleteSubject(req.query.id,result=>{
                                    if(!result){
                                        res.status(204).send('Successfully deleted');
                                    }
                                    else{
                                        res.status(500).send('Failed');
                                    }
                                });
                            }
                            else{
                                res.status(400).send("Bad request");
                            }
                            break;
                        case "/api/delete-room":
                            if(req.query.id){
                                rooms.deleteRoom(req.query.id,result=>{
                                    if(!result){
                                        res.status(204).send('Successfully deleted');
                                    }
                                    else{
                                        res.status(500).send('Failed');
                                    }
                                });
                            }
                            else{
                                res.status(400).send("Bad request");
                            }
                            break;
                        case "/api/delete-admin":
                            if(req.query.id){
                                admins.deleteAdmin(req.query.id,result=>{
                                    if(!result){
                                        res.status(204).send('Successfully deleted');
                                    }
                                    else{
                                        res.status(500).send('Failed');
                                    }
                                });
                            }
                            else{
                                res.status(400).send("Bad request");
                            }
                            break
                        case "/api/delete-student-in-class":
                            if(req.query.deleteIDs) {
                                var deleteIDs = JSON.parse(req.query.deleteIDs);
                                classLists.deleteStudentInClass(deleteIDs.classID, deleteIDs.studentID, (deleteError, affectedRows) => {
                                    if(deleteError) {
                                        res.status(500).json({
                                            success: false,
                                            cause: "Server can't delete"
                                        });
                                    } else {
                                        if(affectedRows == 1) {
                                            res.status(204).json({
                                                success: true
                                            })
                                        } else {
                                            res.status(400).json({
                                                success: false,
                                                cause: "ClassID or StudentID not exists."
                                            })
                                        }
                                    }
                                })
                            } else {
                                res.status(400).send("Bad request!");
                            }
                            break;
                        case "/api/get-dashboard-info":
                            students.listStudents((studentList) => {
                                classLists.listStudentsInClassesWithCondition("examCondition=1",(studentInClassList) => {
                                    require("./appserv/examLists").listStudentsInExams((studentInExamList) => {
                                        subjects.listSubjects(subjectList => {
                                            classes.listClasses(classList => {
                                                require("./appserv/shifts").listShifts(shiftList => {
                                                    rooms.listRooms(roomList => {
                                                        res.json({
                                                            studentList: studentList,
                                                            studentInClassList: studentInClassList,
                                                            studentInExamList: studentInExamList,
                                                            subjectList: subjectList,
                                                            classList: classList,
                                                            shiftList: shiftList,
                                                            roomList: roomList
                                                        })
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                            break;
                        default:
                            res.status(404)
                            res.send("API not found!");
                    }
                }
                else{
                    res.status(404)
                    res.send("API not found!");
                }
        }
    } else {
        res.sendStatus(401);
    }
});
  
  // POST method route
app.post('/api/*', function (req, res) {
    switch(req.originalUrl){
        case "/api/update-student-info":
            if(req.user.roleType == "student"){
                students.updateStudent(req.body, req.user.userID, (result)=>{
                    if(!result){
                        res.sendStatus(200);
                    }
                    else{
                        res.sendStatus(400);
                    }
                })
            }
            else if(req.user.roleType == "admin") {
                students.updateStudent(req.body, req.body.studentID, (result)=>{
                    if(!result){
                        res.sendStatus(200);
                    }
                    else{
                        res.sendStatus(400);
                    }
                })
            }
            else{
                res.sendStatus(401);
            }
            break;
        case "/api/register":
            if(req.user.roleType == "student"){
                var studentID = req.user.userID;
                var subList = req.body;
                var result_list = {};
                if(Object.keys(req.body).length==0){
                    res.json(result);
                }
                Object.keys(subList).forEach((subject,i,arr)=>{
                    require("./appserv/examLists").insertExamList({
                        examInfoID: subList[subject].examInfoID,
                        studentID: studentID
                    },result=>{
                        result_list[subject] = result;
                        if(i==arr.length-1){
                            res.json(result_list);
                        }
                    });
                });
            }
            else if(req.user.roleType == "admin"){
                var studentID = req.body.studentID;
                var examInfoID = req.body.examInfoID;
                var result_list = {};
                require("./appserv/examLists").insertExamList({
                    examInfoID: examInfoID,
                    studentID: studentID
                },(result)=>{
                    result_list[examInfoID] = result;
                    res.json(result_list);
                });
            }
            else{
                res.sendStatus(401);
            }
            break;
        case "/api/change-password":
            if(req.user.roleType == "student"){
                if(req.body) {
                    var changePasswordInfo = req.body;
                    users.showSpecificUser(req.user.userID, (userInfo) => {
                        if(users.validatePassword(changePasswordInfo.currentPass, userInfo.saltString, userInfo.hashPass)) {
                            users.changePassword(changePasswordInfo, (successChange) => {
                                if(successChange) {
                                    res.json({
                                        success: successChange,
                                        cause: ""
                                    })
                                } else {
                                    res.json({
                                        success: successChange,
                                        cause: "Server không thể đổi mật khẩu"
                                    })
                                }
                            })
                        } else {
                            res.json({
                                success: false,
                                cause: "Mật khẩu cũ không khớp"
                            })
                        }
                    })
                } else {
                    res.status(400).send("Bad request!");
                }
            }
            else if(req.user.roleType == "admin"){
                if(req.body) {
                    var changePasswordInfo = req.body;
                    users.showSpecificUser(changePasswordInfo.userID, (userInfo) => {
                        if(users.validatePassword(changePasswordInfo.currentPass, userInfo.saltString, userInfo.hashPass)) {
                            users.changePassword(changePasswordInfo, (successChange) => {
                                if(successChange) {
                                    res.json({
                                        success: successChange,
                                        cause: ""
                                    })
                                } else {
                                    res.json({
                                        success: successChange,
                                        cause: "Server không thể đổi mật khẩu"
                                    })
                                }
                            })
                        } else {
                            res.json({
                                success: false,
                                cause: "Mật khẩu cũ không khớp"
                            })
                        }
                    })
                } else {
                    res.status(400).send("Bad request!");
                }
            }
            else{
                res.sendStatus(401);
            }
            break;
        default:
            if(req.user.roleType=="admin"){
                switch(req.originalUrl){
                    case "/api/add-list-student":
                        students.uploadStudentList(req.body, result => {
                            res.json(result);
                        });
                        break;
                    case "/api/add-list-subject":
                        subjects.uploadSubjectList(req.body, result => {
                            res.json(result);
                        });
                        break;
                    case "/api/add-list-room":
                        rooms.uploadRoomList(req.body, result => {
                            res.json(result);
                        });
                        break;
                    case "/api/add-list-class":
                        classes.uploadClassList(req.body, (result) => {
                            res.json(result);
                        })
                        break;
                    case "/api/add-class-list":
                        classLists.uploadStudentInClassList(req.body, result => {
                            res.json(result);
                        })
                        break;
                    case "/api/create-shift":
                        require("./appserv/shifts").insertShift(req.body,(result)=>{
                            if(result){
                                res.json(req.body);
                            }
                            else{
                                res.sendStatus(400);
                            }
                        })
                        break;
                    case "/api/create-exam-info":
                        require("./appserv/examInfos").insertExamInfo(req.body,result=>{
                            if(result){
                                res.sendStatus(200);
                            }
                            else{
                                res.sendStatus(400);
                            }
                        });
                        break;
                    case "/api/add-student-to-class":
                        console.log(req.body);
                        classLists.insertStudentInClass(req.body, (result) => {
                            console.log(result);
                            if(result) {
                                res.status(200).json({
                                    success: result
                                });
                            } else {
                                res.status(400).json({
                                    success: result
                                });
                            }
                        })
                        break; 
                    case "/api/update-student-in-class":
                        var updateInfo = {
                            examCondition: req.body.examCondition
                        }
                        var studentID = req.body.studentID;
                        var classID = req.body.classID;
                        classLists.updateStudentInClass(updateInfo, classID, studentID, (updateError, affectedRows, changedRows) => {
                            if(updateError || affectedRows == 0 || changedRows == 0) {
                                res.json({
                                    success: false
                                })
                            } else {
                                res.json({
                                    success: true
                                })
                            }
                        })
                        break;
                    case "/api/update-subject":
                        var updateInfo = req.body.updateInfo;
                        var subjectID = req.body.subjectID;
                        subjects.updateSubject(updateInfo, subjectID, (updateError, affectedRows, changedRows) => {
                            if(updateError || affectedRows == 0 || changedRows == 0) {
                                res.json({
                                    success: false
                                })
                            } else {
                                res.json({
                                    success: true
                                })
                            }
                        })
                        break;
                    case "/api/update-class":
                        var updateInfo = req.body.updateInfo;
                        var classID = req.body.classID;
                        classes.updateClass(updateInfo, classID, (updateError, affectedRows, changedRows) => {
                            if(updateError || affectedRows == 0 || changedRows == 0) {
                                res.json({
                                    success: false
                                })
                            } else {
                                res.json({
                                    success: true
                                })
                            }
                        })
                        break;
                    case "/api/update-room":
                        var updateInfo = req.body.updateInfo;
                        var roomID = req.body.roomID;
                        rooms.updateRoom(updateInfo, roomID, (updateError, affectedRows, changedRows) => {
                            if(updateError || affectedRows == 0 || changedRows == 0) {
                                res.json({
                                    success: false
                                })
                            } else {
                                res.json({
                                    success: true
                                })
                            }
                        })
                        break;
                    default:
                        res.status(404)
                        res.send("API not found!");
                }
            }
            else{
                res.sendStatus(403);
            }
            break;
    }
})


//Passport login
app.route('/login')
.get((req, res) => {
    if (!req.isAuthenticated())
        res.sendFile(__dirname + "/FrontEnd/login.html");
    else
        res.redirect("home.html");
})
.post(Passport.authenticate('local', {
    failureRedirect: '/login#wrongidentity',
    successRedirect: '/home.html'
    // failureMessage: "Fail you bitch",
    // successMessage: "Success"
}))
// .post((req, res) => console.log(req.body));
 
Passport.use(new LocalStrategy(
    (username, password, done) => {
        // console.log("User: " + username + "\nPass: " + password);
        users.showSpecificUser(username, (userInfo) => {
            if(userInfo == false) {
                return done(400, null);
            } else if (userInfo === undefined) {
                return done(null, false);
            } else {
                var validateSuccess = users.validatePassword(password, userInfo.saltString, userInfo.hashPass);
                if(validateSuccess) {
                    var newToken = users.generateRandomToken();
                    var newTokenInfo = {
                        "token": newToken
                    };
                    users.updateUser(newTokenInfo, userInfo.userID, (updateError, affectedRows, changedRows) => {
                        if(updateError) {
                            console.log("Update token failed for user: " + userInfo.userID);
                        } else if (affectedRows == 1 && changedRows == 1) {
                            // console.log("success");
                            userInfo.token = newToken;
                            delete userInfo.hashPass;
                            delete userInfo.saltString;
                            return done(null, userInfo);
                        } 
                    })
                } else {
                    return done(null, false);
                }
            }
        })
    }
))

Passport.serializeUser((user, done) => {
    done(null, user);
})

Passport.deserializeUser((user, done) => {
    users.showSpecificUser(user.userID, (userInfo) => {
        if(userInfo !== undefined) {
            return done(null, userInfo);
        } else {
            return done(null, false);
        }
    })
}) 

app.get('/',function (req, res) {
    res.redirect("home.html");
});

app.get('/logout',(req,res)=>{
    req.logout();
    res.redirect("login");
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

