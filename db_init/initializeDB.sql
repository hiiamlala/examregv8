CREATE DATABASE ExamReg;
USE ExamReg;

CREATE TABLE students (
    studentID VARCHAR(30) NOT NULL PRIMARY KEY,
    lastName VARCHAR(30) NOT NULL,
    firstName VARCHAR(30) NOT NULL,
    dob VARCHAR(30) NOT NULL,
    sex VARCHAR(30) NOT NULL,
    yearClass VARCHAR(30) NOT NULL,
    hometown VARCHAR(30),
    addr VARCHAR(100),
    phone VARCHAR(30),
    email VARCHAR(30) NOT NULL
);

CREATE TABLE admins (
    adminID VARCHAR(30) NOT NULL PRIMARY KEY,
    lastName VARCHAR(30) NOT NULL,
    firstName VARCHAR(30) NOT NULL,
    dob VARCHAR(30) NOT NULL,
    sex VARCHAR(30) NOT NULL
);

CREATE TABLE rooms (
    roomID INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    roomNumber VARCHAR(30) NOT NULL,
    roomAddress VARCHAR(30) NOT NULL,
    slots INT(10) NOT NULL
);

CREATE TABLE shifts (
    shiftID INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    startTime VARCHAR(30) NOT NULL,
    examDay VARCHAR(30) NOT NULL
);

CREATE TABLE subjects (
    subjectID VARCHAR(30) NOT NULL PRIMARY KEY,
    subjectName VARCHAR(30) NOT NULL,
    tins INT(10) NOT NULL,
    examDuration INT(10) NOT NULL
);

CREATE TABLE classes (
    classID VARCHAR(30) NOT NULL PRIMARY KEY,
    subjectID VARCHAR(30) NOT NULL,
    lecturer VARCHAR(30) NOT NULL,
    semester INT(10) NOT NULL,
    schoolYear VARCHAR(30) NOT NULL,
    studyRoom VARCHAR(30),
    CONSTRAINT fk_classes_subjects FOREIGN KEY (subjectID) REFERENCES subjects (subjectID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE users (
    userID VARCHAR(30) NOT NULL PRIMARY KEY,
    /*passwd VARCHAR(100) NOT NULL,*/
    hashPass VARCHAR(1024) NOT NULL,
    saltString VARCHAR(100) NOT NULL,
    token VARCHAR(100),
    roleType VARCHAR(30) NOT NULL,
    userStatus VARCHAR(30) NOT NULL
);

CREATE TABLE class_list (
    classID VARCHAR(30) NOT NULL,
    studentID VARCHAR(30) NOT NULL,
    SBD INT(10) NOT NULL,
    examCondition BOOLEAN NOT NULL DEFAULT TRUE,
    PRIMARY KEY (classID, studentID),
    CONSTRAINT fk_classList_classes FOREIGN KEY (classID) REFERENCES classes (classID) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT fk_classList_students FOREIGN KEY (studentID) REFERENCES students (studentID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE exam_info (
    examInfoID INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    subjectID VARCHAR(30) NOT NULL,
    roomID INT(10) NOT NULL,
    shiftID INT(10) NOT NULL,
    CONSTRAINT fk_examInfo_subjects FOREIGN KEY (subjectID) REFERENCES subjects (subjectID) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT fk_examInfo_rooms FOREIGN KEY (roomID) REFERENCES rooms (roomID) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT fk_examInfo_shifts FOREIGN KEY (shiftID) REFERENCES shifts (shiftID) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE exam_list (
    examInfoID INT(10) NOT NULL,
    studentID VARCHAR(30) NOT NULL,
    PRIMARY KEY (examInfoID, studentID),
    CONSTRAINT fk_examList_examInfo FOREIGN KEY (examInfoID) REFERENCES exam_info (examInfoID) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT fk_examList_students FOREIGN KEY (studentID) REFERENCES students (studentID) ON DELETE RESTRICT ON UPDATE CASCADE
);

/*Admin create*/
insert into admins(adminID, lastName, firstName, dob, sex) VALUES ("helo", "Admin", "Pro", "1/1/1970", "Other"); 