(function() {
    'use strict';
    window.addEventListener('load', function() {
      document.getElementById("username").focus();
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
        form.addEventListener("keydown", function(e) {
          if (e.keyCode == 13) {
            e.preventDefault();
            e.stopPropagation();
            // return false;
          }
        }, false);
      });
    }, false);
  })();


document.getElementById("username").onkeydown = function(e) {
  keyDown(e, this, "password");
}

document.getElementById("password").onkeydown = function(e) {
  keyDown(e, this, null);
}

document.getElementById("btn-login").onclick = function() {
  Accept();
}

function keyDown(e, myself, nextControlId) {
  if (window.event) e = window.event; //de chay ca tren IE
  if (e.keyCode == 13) {
    if (myself.id == "password") {
			Accept();
		}
		else {
			document.getElementById(nextControlId).focus();
		}
  }
} 

function Accept() {
  if (document.getElementById("username").value == "" || document.getElementById("password").value == "") {
    
    var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('keydown', function(event) {
          if (event.keyCode == 13 && form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false)});
    // var validation = Array.prototype.filter.call(forms, function(form) {
    //   form.checkValidity();
    // })


    return false;
  }
  // console.log("Submitting....");
  // document.getElementById("form-login").submit();
  var xml = new XMLHttpRequest();
  // var form = new FormData();
  // form.append("username", document.getElementById("username").value);
  // form.append("password", document.getElementById("password").value);
  xml.onloadend = function() {
    window.location.href = this.responseURL;
  }
  xml.open("POST", "/login", true);
  xml.setRequestHeader("Content-Type", "application/json");
  xml.send(JSON.stringify({
    "username": document.getElementById("username").value,
    "password": document.getElementById("password").value
  }));
  // xml.send(form);
}