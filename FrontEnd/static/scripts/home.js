$('document').ready(function () {
  var xhttp = new XMLHttpRequest();
  xhttp.onloadend = function(){
    if(this.status == 200){
      var data = JSON.parse(this.responseText);
      if(data.role || data.role.length != 0){
        window.localStorage.setItem("user",this.responseText);
        init(data.role);
        $('#username').html(data.info.lastName + " " + data.info.firstName);
        $('#userid').html(data.ID);
        if(window.location.hash.length == 0){
          var user = JSON.parse(window.localStorage.getItem("user"));
          if(user.role == "student"){
              window.location.hash = "register";
              $('#main-content').attr('src',window.location.hash.slice(1)+'.html');
          }
          else if(user.role == "admin"){
              window.location.hash = "homepage";
              $('#main-content').attr('src',window.location.hash.slice(1)+'.html');
          }
        }
      }
      else{
        window.localStorage.removeItem("user");
        window.location.href = "/api/logout";
      }
    }
    else if(this.status == 401){
      window.location.href = "login";
    }
    else{
      console.log(this);
    }
  }
  xhttp.open("GET","/api/whoami",true);
  xhttp.send();

  $('.li-menu-item').click(function () {
    $('.li-menu-item').removeAttr('style');
    $(this).css("background-color", "#8b99a791");
    $(this).css("color", "#2c2e3e !important");

  });

  $('.ic_arrow').click(function () {
    $(".dropdown_wrapper").toggle();
  });
  $('.close_option').click(function () {
    $(".dropdown_wrapper").hide();
  });

  $('[data-tooltip="tooltip"]').tooltip();

})

function init(role){
  $('.student-role').hide();
  $('.admin-role').hide();
  $('.root-role').hide();
  $(`.${role}-role`).show();
}