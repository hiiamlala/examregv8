$('document').ready(function(){
    $('#import').on('click',function(){
        $('#main-content').fadeOut(200);
        $('#excel-container').fadeIn(200);
    });
    $('#validate-btn').on('click',function(){
        var formdata = objectifyForm($('#form_1').serializeArray());
        console.log(formdata);
        
        if(!(formdata.roomNumber==""||formdata.roomAddress==""||isNaN(formdata.slots))){
            let room = new Room(formdata.roomNumber,formdata.roomAddress,formdata.slots);
            let xhttp = new XMLHttpRequest();
            xhttp.onloadend = function(){
                if(this.status == 200){
                    window.location.reload();
                }
                else{
                    console.log(this);
                }
            };
            xhttp.open("POST","/api/add-list-room",true);
            xhttp.setRequestHeader('Content-Type', 'application/json');
            xhttp.send(JSON.stringify([room]));
        }
    })
});

function objectifyForm(formArray) {//serialize data function

    var returnArray = {};
    for (var i = 0; i < formArray.length; i++){
      returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}