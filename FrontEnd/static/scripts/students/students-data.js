var table;
$('document').ready(function(){
    initData();
})

function render(data){
    if ($.fn.dataTable.isDataTable('#data_table')) {
        table.destroy();
    }
    table = $('#data_table').DataTable({
        data: data,
        columns: [{
            title: 'Mã sinh viên',
            data: 'studentID'
        },{
            title: 'Họ và tên',
            render: (data, type, row, meta)=>{
                return row.lastName + " " + row.firstName;
            }
        },{
            title: 'Ngày sinh',
            type: 'date',
            data: 'dob'
        },{
            title: 'Giới tính',
            data: 'sex'
        },{
            title: 'Lớp niên khóa',
            data: 'yearClass'
        },{
            title: 'Modify',
            render: (data, type, row, meta)=>{
                return `<button data-toggle="tooltip" title="Xem thêm" type="button" data-target="${row.studentID}" action="info" class="btn-icon"><span class="info_icon"></span></button>
                        <button data-toggle="tooltip" title="Xóa" type="button" data-target="${row.studentID}" action="remove" class="btn-icon"><span class="delete_icon"></span></button>`
            },
            type: "html",
            orderable: false
        }]
    });
    $('#data_table tbody').unbind();
    $('#data_table tbody').on('click', 'button', function () {
        var target = $(this).attr('data-target');
        switch($(this).attr('action')){
            case "remove":
                if(window.confirm(`Are you sure?\nStudent ${$(this).attr('data-target')} will be permanently remove!`)){
                    var xhttp = new XMLHttpRequest();
                    xhttp.onloadend = function(){
                        if(this.status == 204){
                            //noti deleted
                            window.top.$.notify({
                                title: '<strong>Thành công</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Xóa sinh viên thành công!" //message
                              },{
                                type: 'info',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                              });
                            initData();
                        }
                        else if(this.status == 401){
                            window.top.location.href = "login";
                        }
                        else{
                            //noti fail
                            window.top.$.notify({
                                title: '<strong>Thất bại</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Xóa sinh viên không thành công!" //message
                              },{
                                type: 'info',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                              });
                            initData();
                        }
                    }
                    xhttp.open('GET',`/api/delete-student?id=${target}`,true);
                    xhttp.send();
                }
                break;
            case "info":
                window.top.location.href = `home.html?studentID=${target}#student-info`;
                break;
        }
    });
}

function initData(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            var data = JSON.parse(this.responseText);
            render(data);
        }
        else if(this.status == 401){
            window.top.location.href = "login";
        }
        else{
            console.log(this);
        }
    }
    xhttp.open('GET','/api/list-students',true);
    xhttp.send();
}