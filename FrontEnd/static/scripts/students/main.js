$('document').ready(function(){
    $('#import').on('click',function(){
        $('#main-content').fadeOut(200);
        $('#excel-container').fadeIn(200);
    });

    $('#validate-btn').on('click',function(){
        var formdata = objectifyForm($('#form_1').serializeArray());
        if(!(formdata.studentID==""||formdata.firstName==""||formdata.lastName==""||formdata.dob==""||formdata.yearClass=="")){
            let student = new Student(formdata.studentID,formdata.firstName,formdata.lastName,formdata.dob,formdata.sex,formdata.yearClass,formdata.hometown,formdata.addr,formdata.phone,formdata.email);
            let xhttp = new XMLHttpRequest();
            xhttp.onloadend = function(){
                if(this.status == 200){
                    window.location.reload();
                }
                else{
                    console.log(this);
                }
            };
            xhttp.open("POST","/api/add-list-student",true);
            xhttp.setRequestHeader('Content-Type', 'application/json');
            xhttp.send(JSON.stringify([student]));
        }
    })
});

function objectifyForm(formArray) {//serialize data function

    var returnArray = {};
    for (var i = 0; i < formArray.length; i++){
      returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
  }