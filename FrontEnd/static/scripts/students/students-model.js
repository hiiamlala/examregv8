class Student {
    constructor(studentID,firstName,lastName,dob,sex,yearClass,hometown,addr,phone,email){
        this.studentID = studentID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.sex = sex;
        this.yearClass = yearClass;
        this.hometown = hometown;
        this.addr = addr;
        this.phone = phone;
        this.email = email;
    }
    addFullName(fname){
        var temp = fname.split(' ');
        this.firstName = temp[temp.length-1];
        delete temp[temp.length-1];
        this.lastName = temp.join(' ').slice(0,-1);
    }
    toJSON(){
        return{
            studentID: this.studentID,
            firstName: this.firstName,
            lastName: this.lastName,
            dob: this.dob,
            sex: this.sex,
            yearClass: this.yearClass,
            hometown: this.hometown,
            addr: this.addr,
            phone: this.phone,
            email: this.email
        }
    }
}