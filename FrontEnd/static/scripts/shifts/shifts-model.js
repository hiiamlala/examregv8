class Shift {
    constructor(shiftID,startTime,examDay){
        this.shiftID = shiftID;
        this.startTime = startTime;
        this.examDay = examDay;
    }
    toJSON(){
        return{
            shiftID: this.shiftID,
            startTime: this.startTime,
            examDay: this.examDay,
        }
    }
}