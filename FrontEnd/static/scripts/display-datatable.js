$('document').ready(function(){
    $('.open-dropdown').click(function(){
        $(".dropdown_wrapper").toggle();
        
    });
    $('#cancel-dropdown').click(function(){
        $(".dropdown_wrapper").hide();
    });
    $('#close-edition').click(function(){
        $(".edit-dialog").hide();
        $(".content .portlet").show();
    });
    $('#add').click(function(){
        $(".input-method").show();
        $(".dropdown_wrapper").hide();
    });
    $('#close-btn').click(function(){
        $(".input-method").hide();
    });
    $("#validate-btn").click(function (){
        var input=$('[check="required"]');
        $.each(input, function (index, item) {
            var value = $(this).val().trim();
            
                if (value.length == 0) {
                    $(item).parents(".check").append('<div class="err-notis"> Trường này không được để trống</div>');
                    $(item).addClass('border-error');
                    $('.alert_text').show();
                }
                else {
                    $(item).removeClass('border-error');
                    $(item).parents(".check").children('.err-notis').remove();
                }
    
        })
    });
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
      });
})
