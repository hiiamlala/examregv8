var table;
$('document').ready(function(){
    initData();
})

function render(data){
    if ($.fn.dataTable.isDataTable('#data_table')) {
        table.destroy();
    }
    table = $('#data_table').DataTable({
        data: data,
        columns: [
        {
            title: 'ID',
            type: 'num',
            data: 'roomID',
            visible: false
        },{
            title: 'Số phòng',
            type: 'num',
            data: 'roomNumber',
            orderable: false
        },{
            title: 'Địa điểm',
            data: 'roomAddress'
        },{
            title: 'Số máy',
            type: 'num',
            data: 'slots'
        },{
            title: 'Modify',
            render: (data, type, row, meta)=>{
                // return `<button type="button" data-toggle="tooltip" title="Xem thêm" data-target="${row.roomID}" detail="${row.roomNumber + ", " + row.roomAddress}" action="info" class="btn-icon"><span class="info_icon"></span></button>
                //         <button type="button" data-toggle="tooltip" title="Sửa" data-target="${row.subjectID}" action="edit" class="btn-icon"><span class="edit_icon"></span></button>
                //         <button type="button" data-toggle="tooltip" title="Xóa" data-target="${row.roomID}" detail="${row.roomNumber + ", " + row.roomAddress}" action="remove" class="btn-icon"><span class="delete_icon"></span></button>`
                return `<button type="button" data-toggle="tooltip" title="Sửa" data-target="${row.roomID}" action="edit" class="btn-icon"><span class="edit_icon"></span></button>
                        <button type="button" data-toggle="tooltip" title="Xóa" data-target="${row.roomID}" detail="${row.roomNumber + ", " + row.roomAddress}" action="remove" class="btn-icon"><span class="delete_icon"></span></button>`
            },
            type: "html",
            orderable: false
        }]
    });
    $('#data_table tbody').unbind();
    $('#data_table tbody').on('click', 'button', function () {
        var target = $(this).attr('data-target');
        switch($(this).attr('action')){
            case "remove":
                if(window.confirm(`Are you sure?\nRoom ${$(this).attr('detail')} will be permanently remove!`)){
                    var xhttp = new XMLHttpRequest();
                    xhttp.onloadend = function(){
                        if(this.status == 204){
                            //noti deleted
                            successNoti("Xóa phòng thi thành công!")
                            initData();
                        }
                        else if(this.status == 401){
                            window.top.location.href = "login";
                        }
                        else{
                            failNoti("Xóa phòng thi thất bại")
                        }
                    }
                    xhttp.open('GET',`/api/delete-room?id=${target}`,true);
                    xhttp.send();
                }
                break;
            case "edit":
                $(".content .portlet").hide();
                $(".edit-dialog").show();
                $('#edit-roomID').val(target);
                for(let i = 0; i < data.length; i++) {
                    if(data[i].roomID == target) {
                        $('#edit-roomNumber').val(data[i].roomNumber);
                        $('#edit-roomAddress').val(data[i].roomAddress);
                        $('#edit-slots').val(data[i].slots);
                    }
                    break;
                }
                $('#save-edition').click(function() {
                    updateRoom(target);
                })
                break;
            case "info":
                //Jump to student page
                break;
        }
    });
};

function initData(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            var data = JSON.parse(this.responseText);
            render(data);
        }
        else if(this.status == 401){
            window.top.location.href = "login";
        }
        else{
            console.log(this);
        }
    }
    xhttp.open('GET','/api/list-rooms',true);
    xhttp.send();
}

function updateRoom(target) {
    var updateRequest = {
        roomID: target,
        updateInfo: {
            roomNumber: $('#edit-roomNumber').val(),
            roomAddress: $('#edit-roomAddress').val(),
            slots: $('#edit-slots').val()
        }
    }

    var xml = new XMLHttpRequest();

    xml.onloadend = function() {
        var data = JSON.parse(this.responseText);
        if(data.success) {
            successNoti("Cập nhật phòng thi thành công");
            window.location.reload();
        } else {
            failNoti("Cập nhật phòng thi thất bại");
        }
    }

    xml.open("POST", "api/update-room", true);
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(updateRequest));
}