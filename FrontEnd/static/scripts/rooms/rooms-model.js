class Room {
    constructor(roomID,roomNumber,roomAddress,slots){
        this.roomID = roomID
        this.roomNumber = roomNumber;
        this.roomAddress = roomAddress;
        this.slots = slots;
    }
    toJSON(){
        return{
            roomID: this.roomID,
            roomNumber: this.roomNumber,
            roomAddress: this.roomAddress,
            slots: this.slots,
        }
    }
}