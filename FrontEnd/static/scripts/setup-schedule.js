var exam_info = new Exam_info(null,null,null);
var subject;
var room;
var shift;

initSubjectData();

$('#step-1').click(function(){
  $(".content-step-2").hide();
  $(".content-step-3").hide();
  $(".content-step-4").hide();
  $(".content-step-1").show();
});
$('#step-2').click(function(){
  $(".content-step-1").hide();
  $(".content-step-3").hide();
  $(".content-step-4").hide();
  $(".content-step-2").show();
});
$('#step-3').click(function(){
  $(".content-step-2").hide();
  $(".content-step-1").hide();
  $(".content-step-4").hide();
  $(".content-step-3").show();
});
$('#step-4').click(function(){
  $(".content-step-2").hide();
  $(".content-step-3").hide();
  $(".content-step-1").hide();
  $(".content-step-4").show();
});

function renderSubject(data){
  if ($.fn.dataTable.isDataTable('#data_table')) {
      table.destroy();
  }
  table = $('#subject_data_table').DataTable({
      data: data,
      columns: [{
          title: 'Mã môn học',
          data: 'subjectID'
      },{
          title: 'Tên môn học',
          data: 'subjectName'
      },{
          title: 'Số tín chỉ',
          data: 'tins',
          type: 'num'
      },{
          title: 'Thời gian thi',
          type: 'num',
          data: 'examDuration'
      },{
        title: 'Số sinh viên',
        type: 'num',
        data: 'studentNum'
      },{
          render: (data, type, row, meta)=>{
              return `<button type="button" data-type="subject" data-target="${row.subjectID}" data-detail="${row.subjectName}|${row.tins}|${row.examDuration}|${row.studentNum}" action="select" class="btn btn-primary ">Select</button>`
          },
          type: "html",
          orderable: false
      }]
  });
  $('#subject_data_table tbody').unbind();
  $('#subject_data_table tbody').on('click', 'button', function () {
    if($(this).attr('data-type') == "subject"){
      var target = $(this).attr('data-target');
      var detail = $(this).attr('data-detail').split("|");
      $(".setup-info").show();
      $('#step1-next').click();
      exam_info.subjectID = target;
      subject = new Subject(target,detail[0],detail[1],detail[2]);
      $('#shift-time-start').unbind();
      $('#shift-time-start').on('change',function(){
        $('#shift-time-end').val(endTime($('#shift-time-start').val(),subject.examDuration));
      });
      $(".content-step-1").hide();
      $(".content-step-2").show();
      $("#subject").html(`${target} | ${detail[0]}`);
      $("#studentNum").html(detail[3]);
      $("#examDuration").html(detail[2]);
      $('#step-2').addClass("active");
      initRoomData();
    }
  });
}

function endTime(start, examDuration){
  var time = start.split(':');
  var hour,min;
  hour = parseInt(time[0])+Math.floor(examDuration/60);
  min = parseInt(time[1])+(examDuration%60);
  if(min>=60){
    min=min%60;
    hour+=Math.floor(min/60);
  }
  return `${("0"+hour%24).slice(-2)}:${("0"+min).slice(-2)}`;
}

function renderRoom(data){
  if ($.fn.dataTable.isDataTable('#room_data_table')) {
      table.destroy();
  }
  table = $('#room_data_table').DataTable({
      data: data,
      columns: [
        {
            title: 'ID',
            type: 'num',
            data: 'roomID',
            visible: false
        },{
            title: 'Số phòng',
            type: 'num',
            data: 'roomNumber',
            orderable: false
        },{
            title: 'Địa điểm',
            data: 'roomAddress'
        },{
            title: 'Số chỗ trống',
            type: 'num',
            data: 'slots'
        },{
          render: (data, type, row, meta)=>{
              return `<button type="button" data-type="room" data-target="${row.roomID}" data-detail="${row.roomNumber}|${row.roomAddress}|${row.slots}" action="select" class="btn btn-primary ">Select</button>`
          },
          type: "html",
          orderable: false
      }]
  });
  $('#room_data_table tbody').unbind();
  $('#room_data_table tbody').on('click', 'button', function () {
    if($(this).attr('data-type')=="room"){
      var target = $(this).attr('data-target');
      var detail = $(this).attr('data-detail').split("|");
      exam_info.roomID = target;
      room = new Room(target,detail[0],detail[1],detail[2]);
      $(".content-step-2").hide();
      $(".content-step-3").show();
      $('#step-3').addClass("active");
      $('#step-2').removeClass("active");
      initShiftData();
    }
  });
}

function renderShift(data){
  if ($.fn.dataTable.isDataTable('#shift_data_table')) {
      table.destroy();
  }
  table = $('#shift_data_table').DataTable({
      data: data,
      columns: [
        {
            title: 'ID',
            type: 'num',
            data: 'shiftID',
            visible: false
        },{
            title: 'Ngày thi',
            type: 'date',
            data: 'examDay',
            orderable: false
        },{
            title: 'Thời gian thi',
            data: 'startTime'
        },{
          render: (data, type, row, meta)=>{
            if(row.avail.available){
              return `<button type="button" data-type="shift" data-target="${row.shiftID}" data-detail="${row.examDay}|${row.startTime}" action="select" class="btn btn-primary ">Select</button>`
            }
            else{
              return `Ca thi bị trùng`
            }
          },
          type: "html",
          orderable: false
      }]
  });
  $('#shift_data_table tbody').unbind();
  $('#shift_data_table tbody').on('click', 'button', function () {
    if($(this).attr('data-type')=="shift"){
      var target = $(this).attr('data-target');
      var detail = $(this).attr('data-detail').split("|");
      exam_info.shiftID = target;
      shift = new Shift(target,detail[1],detail[0]);
      $(".content-step-3").hide();
      $(".content-step-4").show();
      $('#step-4').addClass("active");
      $('#step-3').removeClass("active");
      $('#schedule-result').html(`
        <h5>Môn học: ${subject.subjectID} | ${subject.subjectName} </h3>
        <div>Địa điểm thi: ${room.roomNumber}, ${room.roomAddress} </div>
        <div> Số máy: ${room.slots} </div>
        <div>Ca thi: Ngày ${shift.examDay} từ ${shift.startTime} đến ${endTime(shift.startTime,subject.examDuration)} </div>
        <br>
      `);
      $('#save-exam-info').unbind();
      $('#save-exam-info').attr("disabled",false);
      $('#save-exam-info').on('click',function(){
        var xhttp = new XMLHttpRequest();
        xhttp.onloadend = function(){
          if(this.status==200){
            window.top.$.notify({
              title: '<strong>Thành công</strong>', //Tên thông báo
              icon: 'glyphicon glyphicon-star',
              message: "Tạo ca thi thành công!" //message
            },{
              type: 'info',
              animate: {
                    enter: 'animated fadeInUp',
                exit: 'animated fadeOutRight'
              },
              placement: {
                from: "bottom",
                align: "left"
              },
              offset: 20,
              spacing: 10,
              z_index: 1031,
            });
            window.location.reload();
          }
          else if(this.status == 401){
            window.top.location.href = "login";
          }
          else{
            console.log(this);
            window.alert(this.responseText);
          }
        }
        xhttp.open("POST","/api/create-exam-info",true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xhttp.send(JSON.stringify(new Exam_info(subject.subjectID,room.roomID,shift.shiftID)));
        console.log(new Exam_info(subject.subjectID,room.roomID,shift.shiftID));
      })
    }
  });
}

$('#shift-check').on('click',function(){
  if($('#shift-time-start').val()!=""&&$('#shift-date').val()!=""){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
      if(this.status == 200){
        initShiftData();
        window.top.$.notify({
          title: '<strong>Thành công</strong>', //Tên thông báo
          icon: 'glyphicon glyphicon-star',
          message: "Tạo ca thi thành công!" //message
        },{
          type: 'info',
          animate: {
                enter: 'animated fadeInUp',
            exit: 'animated fadeOutRight'
          },
          placement: {
            from: "bottom",
            align: "left"
          },
          offset: 20,
          spacing: 10,
          z_index: 1031,
        });
      }
      else if(this.status == 401){
        window.top.location.href = "login";
      }
      else{
        window.alert(this.responseText);
      }
    }
    xhttp.open("POST","/api/create-shift",true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify({
      startTime: $('#shift-time-start').val(),
      examDay: $('#shift-date').val()
    }));
  }
  else{
    window.alert("fill");
  }
})

function initShiftData(){
  var xhttp = new XMLHttpRequest();
  xhttp.onloadend = function(){
      if(this.status == 200){
          var data = JSON.parse(this.responseText);
          renderShift(data);
      }
      else if(this.status == 401){
        window.top.location.href = "login";
      }
      else{
          console.log(this);
          window.alert(this.responseText);
      }
  },
  xhttp.open('GET','/api/list-shifts?roomID='+exam_info.roomID+"&subjectID="+exam_info.subjectID,true);
  xhttp.send();
}

function initSubjectData(){
  var xhttp = new XMLHttpRequest();
  xhttp.onloadend = function(){
      if(this.status == 200){
          var data = JSON.parse(this.responseText);
          renderSubject(data);
      }
      else if(this.status == 401){
        window.top.location.href = "login";
      }
      else{
          console.log(this);
          window.alert(this.responseText);
      }
  }
  xhttp.open('GET','/api/list-subjects',true);
  xhttp.send();
}

function initRoomData(){
  var xhttp = new XMLHttpRequest();
  xhttp.onloadend = function(){
      if(this.status == 200){
          var data = JSON.parse(this.responseText);
          renderRoom(data);
      }
      else if(this.status == 401){
        window.top.location.href = "login";
      }
      else{
          console.log(this);
          window.alert(this.responseText);
      }
  }
  xhttp.open('GET','/api/list-rooms',true);
  xhttp.send();
}

