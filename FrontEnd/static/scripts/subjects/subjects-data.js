var table;
$('document').ready(function(){
    initData();
})

function render(data){
    if ($.fn.dataTable.isDataTable('#data_table')) {
        table.destroy();
    }
    table = $('#data_table').DataTable({
        data: data,
        columns: [{
            title: 'Mã môn học',
            data: 'subjectID'
        },{
            title: 'Tên môn học',
            data: 'subjectName'
        },{
            title: 'Số tín chỉ',
            data: 'tins',
            type: 'num'
        },{
            title: 'Thời gian thi',
            type: 'num',
            data: 'examDuration'
        },{
            title: 'Modify',
            render: (data, type, row, meta)=>{
                return `<button type="button" data-toggle="tooltip" title="Xem thêm" data-target="${row.subjectID}" action="info" class="btn-icon"><span class="info_icon"></span></button>
                        <button type="button" data-toggle="tooltip" title="Sửa" data-target="${row.subjectID}" action="edit" class="btn-icon"><span class="edit_icon"></span></button>
                        <button type="button" data-toggle="tooltip" title="Xóa" data-target="${row.subjectID}" action="remove" class="btn-icon"><span class="delete_icon"></span></button>`
            },
            type: "html",
            orderable: false
        }]
    });
    $('#data_table tbody').unbind();
    $('#data_table tbody').on('click', 'button', function () {
        var target = $(this).attr('data-target');
        switch($(this).attr('action')){
            case "remove":
                if(window.confirm(`Are you sure?\nSubject ${$(this).attr('data-target')} will be permanently remove!`)){
                    var xhttp = new XMLHttpRequest();
                    xhttp.onloadend = function(){
                        if(this.status == 204){
                            //noti deleted
                            successNoti("Xóa môn học thành công!");
                            initData();
                        }
                        else if(this.status == 401){
                            window.top.location.href = "login";
                        }
                        else{
                            //noti fail
                            window.top.$.notify({
                                title: '<strong>Thất bại</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Xóa môn không thành công!" //message
                              },{
                                type: 'danger',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                              });
                            initData();
                        }
                    }
                    xhttp.open('GET',`/api/delete-subject?id=${target}`,true);
                    xhttp.send();
                }
                break;
            case "edit":
                $(".content .portlet").hide();
                $(".edit-dialog").show();
                $('#edit-subjectID').val(target);
                for(let i = 0; i < data.length; i++) {
                    if(data[i].subjectID == target) {
                        $('#edit-subjectName').val(data[i].subjectName);
                        $('#edit-tins').val(data[i].tins);
                        $('#edit-examDuration').val(data[i].examDuration);
                    }
                    break;
                }
                $('#save-edition').click(function() {
                    updateSubject(target);
                })
                break;
            case "info":
                window.parent.location.href = `home.html?subjectID=${target}#class-list`;
                break;

        }
    });
}

function initData(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            var data = JSON.parse(this.responseText);
            render(data);
        }
        else if(this.status == 401){
            window.top.location.href = "login";
        }
        else{
            console.log(this);
            window.alert('Error when geting data');
        }
    }
    xhttp.open('GET','/api/list-subjects',true);
    xhttp.send();
}

$('#validate-btn').on('click',function(){
    console.log($('#form_1').serializeArray());
})

function updateSubject(target) {
    var updateRequest = {
        subjectID: target,
        updateInfo: {
            tins: $('#edit-tins').val(),
            examDuration: $('#edit-examDuration').val()
        }
    }

    var xml = new XMLHttpRequest();

    xml.onloadend = function() {
        var data = JSON.parse(this.responseText);
        if(data.success) {
            successNoti("Cập nhật môn học thành công");
            window.location.reload();
        } else {
            failNoti("Cập nhật môn học thất bại");
        }
    }

    xml.open("POST", "api/update-subject", true);
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(updateRequest));
}