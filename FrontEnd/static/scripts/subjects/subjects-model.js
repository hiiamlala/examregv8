class Subject {
    constructor(subjectID,subjectName,tins,examDuration){
        this.subjectID = subjectID;
        this.subjectName = subjectName;
        this.tins = tins;
        this.examDuration = examDuration;
    }
    toJSON(){
        return{
            subjectID: this.subjectID,
            subjectName: this.subjectName,
            tins: this.tins,
            examDuration: this.examDuration,
        }
    }
}