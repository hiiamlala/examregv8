$('document').ready(function(){
    $('#import').on('click',function(){
        $('#main-content').fadeOut(200);
        $('#excel-container').fadeIn(200);
    });
    $('#validate-btn').on('click',function(){
        var formdata = objectifyForm($('#form_1').serializeArray());
        if(!(formdata.subjectID==""||formdata.subjectName==""||formdata.tins==""||formdata.examDuration=="")){
            let subject = new Subject(formdata.subjectID,formdata.subjectName,formdata.tins,formdata.examDuration);
            let xhttp = new XMLHttpRequest();
            xhttp.onloadend = function(){
                if(this.status == 200){
                    window.location.reload();
                }
                else{
                    console.log(this);
                }
            };
            xhttp.open("POST","/api/add-list-subject",true);
            xhttp.setRequestHeader('Content-Type', 'application/json');
            xhttp.send(JSON.stringify([subject]));
        }
    })
});

function objectifyForm(formArray) {//serialize data function

    var returnArray = {};
    for (var i = 0; i < formArray.length; i++){
      returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
  }