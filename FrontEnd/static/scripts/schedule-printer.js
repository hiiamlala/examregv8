function exportHTML(){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("print-area").innerHTML+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'document.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
}

function printNow() {
    var printContents = document.getElementById('print-area').innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

var xml = new XMLHttpRequest();

xml.onloadend = function() {
    var data = JSON.parse(this.responseText);
    if(data) {
        $('#studentName').html("<b>" + String(data.info.lastName) + " " + String(data.info.firstName) + "</b>");
        $('#studentDOB').html("<b>" + String(data.info.dob) + "</b>");
        $('#studentID').html("<b>" + String(data.info.studentID) + "</b>");
        $('#studentYearClass').html("<b>" + String(data.info.yearClass) + "</b>");
        $('#studentSignature').html(String(data.info.lastName) + " " + String(data.info.firstName));

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();

        $('#today').html("Ngày " + String(dd) + " tháng " + String(mm) + " năm " + String(yyyy));
        $('#placeAndTime').append("ngày " + String(dd) + " tháng " + String(mm) + " năm " + String(yyyy));

        var getRegistered = new XMLHttpRequest();

        getRegistered.onloadend = function() {
            var dataExamInfo = JSON.parse(this.responseText);
            if (dataExamInfo) {
                $('#numberOfRegister').html(String(dataExamInfo.length));

                dataExamInfo.forEach((examInfo, examInfoIndex, examInfoArray) => {
                    if(examInfoIndex === examInfoArray.length - 1) {
                        $('#examList > tbody > tr').eq(examInfoIndex).after(`
                            <tr>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${examInfoIndex+1}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${examInfo.subjectInfo.subjectID}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${examInfo.subjectInfo.subjectName}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${examInfo.SBD}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${examInfo.roomInfo.roomNumber}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${examInfo.roomInfo.roomAddress}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${examInfo.shiftInfo.startTime}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${examInfo.shiftInfo.examDay}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center; border-right:1px solid #000;">${examInfo.subjectInfo.examDuration} (phút)</th>
                            </tr>
                        `);    
                    } else {
                        $('#examList > tbody > tr').eq(examInfoIndex).after(`
                            <tr>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${examInfoIndex+1}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${examInfo.subjectInfo.subjectID}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${examInfo.subjectInfo.subjectName}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${examInfo.SBD}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${examInfo.roomInfo.roomNumber}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${examInfo.roomInfo.roomAddress}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${examInfo.shiftInfo.startTime}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${examInfo.shiftInfo.examDay}</th>
                                <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center; border-right:1px solid #000;">${examInfo.subjectInfo.examDuration} (phút)</th>
                            </tr>
                        `);
                    }
                })
            } else {
                console.log("get-registered return none");
            }
        }

        getRegistered.open('GET', '/api/get-registered?studentID=' + String(data.info.studentID), true);
        getRegistered.send();
    } else {
        console.log("whoami return none");
    }
}
xml.open('GET','/api/whoami',true);
xml.send();