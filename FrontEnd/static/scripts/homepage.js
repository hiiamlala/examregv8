var students = new XMLHttpRequest();

students.onloadend = function() {
    if(this.status==200){
        var data = JSON.parse(this.responseText);
        $('#students').html(data.studentList.length);
        $('#camthi').html(data.studentInClassList.length);
        $('#regis').html(data.studentInExamList.length);
        $('#subject').html(data.subjectList.length);
        $('#classes').html(data.classList.length);
        $('#shift').html(data.shiftList.length);
        $('#room').html(data.roomList.length);
        var numSlot = 0;
        data.roomList.forEach(room => {
            numSlot += room.slots;
            $('#slot').html(numSlot);
        })
    }
    else{
        window.alert("Không thể lấy dữ liệu dashboard")
    }
}

students.open('GET','/api/get-dashboard-info',true);
students.send();
