if(!window.location.hash && window.location.hash.length == 0){
    if(window.localStorage.getItem("user") && window.localStorage.getItem("user").length!=0){
        var user = JSON.parse(window.localStorage.getItem("user"));
        if(user.role == "student"){
            window.location.hash = "register";
            $('#main-content').attr('src',window.location.hash.slice(1)+'.html');
        }
        else if(user.role == "admin"){
            window.location.hash = "homepage";
            $('#main-content').attr('src',window.location.hash.slice(1)+'.html');
        }
    }
}
else{
    $('#main-content').attr('src',window.location.hash.slice(1)+'.html');
}
window.onhashchange = function( e ) {
    $('#main-content').attr('src',window.location.hash.slice(1)+'.html');
};

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}