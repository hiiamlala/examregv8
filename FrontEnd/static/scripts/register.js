var subject_list = {};
var temporal_reg = {};
var reged = {};

$(document).ready(function(){
    initSubject();
});

function initSubject(){
    temporal_reg = {};
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            var data = JSON.parse(this.responseText);
            data.forEach((ele,i,arr)=>{
                var xhttp = new XMLHttpRequest();
                xhttp.onloadend = function(){
                    if(this.status == 200){
                        var exam_info = JSON.parse(this.responseText);
                        var temp = ele;
                        temp.exams = exam_info;
                        subject_list[ele.subjectID] = temp;
                        if(i==arr.length-1){
                            renderReg(initReged);
                        }
                    }else if(this.status == 401){
                        window.top.location.href = "login"
                    }
                    else{
                        console.log("Ngoài thời gian đăng ký");
                    }
                }
                xhttp.open("GET","/api/list-exam-info-by-subject?subjectID=" + ele.subjectID ,true);
                xhttp.send();
            });
        }else if(this.status == 401){
            window.top.location.href = "login";
        }
        else{
            window.alert("Ngoài thời gian đăng ký");
        }
    }
    xhttp.open("GET","/api/list-exam-subject",true);
    xhttp.send();
}

function renderReg(callback){
    $('#reg-container').html('');
    Object.keys(subject_list).forEach(ele=>{
        var subject = subject_list[ele];
        var table_subject = "table-" + subject.subjectID.replace(" ","-");
        var exam_check_name = ele.replace(" ","-") + "-check";
        var exam_container_id = ele.replace(" ","-") + "-exams";
        $('#reg-container').append(`
        <div class="portlet">
            <div class="portlet-header  inline">
                <div class="subject-name">${ele} | ${subject.subjectName}
                <div id="noti-${subject.subjectID.replace(" ","-")}"></div></div>
                
            </div>
            <div class="portlet-body" id="${table_subject}" style="display:${Object.keys(reged).includes(subject.subjectID)?"none":"block"}">
                <table >
                    <thead>
                    <tr>
                        <th>Phòng</th>
                        <th>Địa điểm</th>
                        <th>Ngày</th>
                        <th>Thời gian vào thi</th>
                        <th>Thời lượng</th>
                        <th>Chọn</th>
                    </tr>
                    </thead>
                    <tbody  id="${exam_container_id}"></tbody>
                </table>
            </div>
        </div>
        `);
        subject.exams.forEach((ele,i,arr)=>{
            $(`#${exam_container_id}`).append(`
                
                    <tr>
                        <td>${ele.roomInfo.roomNumber}</td>
                        <td>${ele.roomInfo.roomAddress}</td>
                        <td>${ele.shiftInfo.examDay}</td>
                        <td>${ele.shiftInfo.startTime}</td>
                        <td>${subject.examDuration}</td>
                        <td><input class="reg" type="radio" name="${exam_check_name}" target="${ele.examInfoID}" subject="${subject.subjectID}" detail="${ele.roomInfo.roomNumber}|${ele.roomInfo.roomAddress}|${ele.shiftInfo.examDay}|${ele.shiftInfo.startTime}"></td>
                    </tr>
                
            `);
            if(i==arr.length-1){
                callback();
            }
        })
    });
    $('.reg').on('click',function(){
        var target = $(this).attr("target");
        var detail = $(this).attr("detail").split("|");
        temporal_reg[$(this).attr("subject")] = {
            examInfoID:target,
            roomNumber: detail[0],
            roomAddress: detail[1],
            examDay: detail[2],
            startTime: detail[3]
        }
    });
}

function initReged(){
    var getRegistered = new XMLHttpRequest();

    getRegistered.onloadend = function() {
        if(this.status == 200){
            reged = {};
            $(`#reged-body`).html("");
            var dataExamInfo = JSON.parse(this.responseText);
            if (dataExamInfo) {
                dataExamInfo.forEach(ele=>{
                    reged[ele.subjectInfo.subjectID] = ele;
                    $(`#table-${ele.subjectInfo.subjectID.replace(" ","-")}`).hide();
                    $(`#noti-${ele.subjectInfo.subjectID.replace(" ","-")}`).html("Bạn đã đăng ký môn này rồi");
                    $(`.reg[target=${ele.examInfoID}]`).attr("checked",true);
                    $(`#reged-body`).append(`
                        <tr>
                            <td>${ele.subjectInfo.subjectID}</td>
                            <td>${ele.subjectInfo.subjectName}</td>
                            <td>${ele.SBD}</td>
                            <td>${ele.roomInfo.roomNumber}</td>
                            <td>${ele.roomInfo.roomAddress}</td>
                            <td>${ele.shiftInfo.examDay}</td>
                            <td>${ele.shiftInfo.startTime}</td>
                            <td>${ele.subjectInfo.examDuration}</td>
                            <td><button type="button" action="dereg" target="${ele.examInfoID}">X</button></td>
                        </tr>
                    `);
                });
                $('button[action=dereg]').unbind();
                $('button[action=dereg]').on('click',function(){
                    var target = $(this).attr("target");
                    var xhttp = new XMLHttpRequest();
                    xhttp.onloadend = function(){
                        reged = {};
                        initSubject();
                        if(this.status == 200){
                            window.top.$.notify({
                                title: '<strong>Thành công</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Đã hủy" //message
                            },{
                                type: 'info',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                        }else if(this.status == 401){
                            window.top.location.href = "login"
                        }
                        else{
                            window.top.$.notify({
                                title: '<strong>Lỗi</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Không hủy được môn" //message
                            },{
                                type: 'danger',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                        }
                    }
                    xhttp.open("GET","/api/dereg?examInfoID="+target,true);
                    xhttp.send();
                });
            }
            else{
                window.top.$.notify({
                    title: '<strong>Lỗi</strong>', //Tên thông báo
                    icon: 'glyphicon glyphicon-star',
                    message: "Không thể lấy danh sách môn đã đăng ký!" //message
                  },{
                    type: 'danger',
                    animate: {
                          enter: 'animated fadeInUp',
                      exit: 'animated fadeOutRight'
                    },
                    placement: {
                      from: "bottom",
                      align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                  });
            }
        }
    }
    getRegistered.open('GET', '/api/get-registered?studentID=' + JSON.parse(localStorage.getItem('user')).ID, true);
    getRegistered.send();
}

$('#reg-butt').on('click',function(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        initSubject();
        if(this.status==200){
            var result = JSON.parse(this.responseText);
            var err = [];
            var success = 0;
            Object.keys(result).forEach(ele=>{
                if(result[ele]==false){
                    err.push(ele);
                }
                else{
                    success++;
                }
            });
            window.top.$.notify({
                title: '<strong>Thông báo</strong>', //Tên thông báo
                icon: 'glyphicon glyphicon-star',
                message: `Đăng ký thành công ${success} môn
                <br>
                Chưa đăng ký được môn ${err}` //message
            },{
                type: 'info',
                animate: {
                      enter: 'animated fadeInUp',
                  exit: 'animated fadeOutRight'
                },
                placement: {
                  from: "bottom",
                  align: "left"
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
            });
        }
        else if(this.status==401){
            window.top.location.href = "login";
        }
        else{
            window.top.$.notify({
                title: '<strong>Lỗi</strong>', //Tên thông báo
                icon: 'glyphicon glyphicon-star',
                message: "Lỗi không xác định" //message
            },{
                type: 'danger',
                animate: {
                      enter: 'animated fadeInUp',
                  exit: 'animated fadeOutRight'
                },
                placement: {
                  from: "bottom",
                  align: "left"
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
            });
        }
    }
    xhttp.open("POST","/api/register",true);
    xhttp.setRequestHeader("Content-type","application/json");
    xhttp.send(JSON.stringify(temporal_reg));
});