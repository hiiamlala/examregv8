var subject_list = [];
var studentID = JSON.parse(window.localStorage.getItem('user')).role=="student"?JSON.parse(window.localStorage.getItem('user')).ID:parseURLParams(window.top.location.href).studentID==undefined?JSON.parse(window.localStorage.getItem('user')).ID:parseURLParams(window.top.location.href).studentID[0];

$('document').ready(function(){
    initStudentInfo();
    initSubjectList();
    initReged();
    $('.open-dropdown').click(function(){
        $(".dropdown_wrapper").toggle();
    });
})
$('#close-btn').click(function(){
    $(".input-method").hide();
});
$("#info-save-btn").click(function (){
    var input=$('[check="required"]');
    var valid = true;
    $.each(input, function (index, item) {
        var value = $(this).val().trim();
        if (value.length == 0) {
            $(item).parents(".check").append('<div class="err-notis"> Trường này không được để trống</div>');
            $(item).addClass('border-error');
            $('.alert_text').show();
            valid = false;
        }
        else {
            $(item).removeClass('border-error');
            $(item).parents(".check").children('.err-notis').remove();
        }
    })
    if(!valid){
        var xhttp = new XMLHttpRequest();
        xhttp.onloadend = function(){
            if(this.status == 200){
                successNoti("Cập nhật thành công");
                window.location.reload();
            }
            else if(this.status == 401){
                window.top.location.href = "login";
            }
            else{
                console.log(this);
            }
        }
        xhttp.open("POST","/api/update-student-info",true);
        xhttp.setRequestHeader("Content-type","application/json");
        xhttp.send(JSON.stringify({
            studentID: studentID,
            lastName : $("#form_1 input[name=last-name]").val(),
            firstName : $("#form_1 input[name=first-name]").val(),
            dob : $("#form_1 input[name=dob]").val(),
            sex : $("#form_1 input[value=male]").is(':checked')?"Nam":"Nữ",
            yearClass : $("#form_1 input[name=year-class]").val(),
            hometown : $("#form_1 input[name=home-town]").val(),
            addr : $("#form_1 input[name=address]").val(),
            phone : $("#form_1 input[name=phone]").val(),
            email : $("#form_1 input[name=email]").val()
        }));
    }
});

$('#pass-save-btn').click(function() {
    if($('#new-pass').val() == $('#retype').val()) {
        var changePasswordInfo = {};
        changePasswordInfo.currentPass = $('#old-pass').val();
        changePasswordInfo.newPass = $('#new-pass').val();
        changePasswordInfo.userID = studentID;
        changePassword(changePasswordInfo);
    } else {
        window.top.$.notify({
            title: '<strong>Lỗi</strong>', //Tên thông báo
            icon: 'glyphicon glyphicon-star',
            message: `Nhập lại mật khẩu mới không khớp` //message
        },{
            type: 'info',
            animate: {
                  enter: 'animated fadeInUp',
              exit: 'animated fadeOutRight'
            },
            placement: {
              from: "bottom",
              align: "left"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
        });
        $('#old-pass').val("");
        $('#new-pass').val("");
        $('#retype').val("");
        $('#old-pass').focus();
    }
})


function initSubjectList(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function() {
        if(this.status == 200){
            subject_list = JSON.parse(this.responseText);
            renderSubjectList(subject_list);
        }
        else if (this.status == 401){
            window.top.location.href = "login";
        }
        else{
            //window.top.location.replace("login");
            console.log(this);
        }
    }
    xhttp.open("GET", "api/list-subjects?studentID="+studentID,true);
    xhttp.send();
}

function renderSubjectList(list){
    $('#subject-data').html('');
    list.forEach(ele=>{
        $('#subject-data').append(`
            <tr>
                <td>${ele.subjectID}</td>
                <td>${ele.subjectName}</td>
                <td>${ele.tins}</td>
            </tr>
        `);
    });
}

function initStudentInfo(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function() {
        if(this.status == 200){
            var student = JSON.parse(this.responseText);
            $("#student-name").html(student.studentID);
            $("#form_1 input[name=last-name]").val(student.lastName);
            $("#form_1 input[name=first-name]").val(student.firstName);
            $("#form_1 input[name=dob]").val(student.dob);
            if(student.sex == "Nữ") $("#form_1 input[value=female]").attr("checked",true);
            $("#form_1 input[name=year-class]").val(student.yearClass);
            $("#form_1 input[name=home-town]").val(student.hometown);
            $("#form_1 input[name=address]").val(student.addr);
            $("#form_1 input[name=phone]").val(student.phone);
            $("#form_1 input[name=email]").val(student.email);
        }
        else if (this.status == 401){
            window.top.location.href = "login";
        }
        else{
            //window.top.location.replace("login");
            console.log(this);
        }
    }
    xhttp.open("GET", "api/get-student-info?id="+studentID,true);
    xhttp.send();
}

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}

function changePassword(changePasswordInfo) {
    var xml = new XMLHttpRequest();

    xml.onloadend = function() {
        if(this.status == 200) {
            var data = JSON.parse(this.responseText);
            if(data) {
                if(data.success) {
                    window.top.$.notify({
                        title: '<strong>Thành công</strong>', //Tên thông báo
                        icon: 'glyphicon glyphicon-star',
                        message: "Đổi mật khẩu thành công" //message
                    },{
                        type: 'info',
                        animate: {
                              enter: 'animated fadeInUp',
                          exit: 'animated fadeOutRight'
                        },
                        placement: {
                          from: "bottom",
                          align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                } else {
                    window.top.$.notify({
                        title: '<strong>Lỗi</strong>', //Tên thông báo
                        icon: 'glyphicon glyphicon-star',
                        message: `Đổi mật khẩu không thành công (${data.cause})` //message
                    },{
                        type: 'info',
                        animate: {
                              enter: 'animated fadeInUp',
                          exit: 'animated fadeOutRight'
                        },
                        placement: {
                          from: "bottom",
                          align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                    });
                }
            } else {
                console.log("change-password return none");
            }
        } else {
            console.log(this);
        }
    }

    xml.open("POST", "api/change-password",true);
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(changePasswordInfo));
}

function initReged(){
    var getRegistered = new XMLHttpRequest();
    getRegistered.onloadend = function() {
        if(this.status == 200){
            $(`#reged-body`).html("");
            var dataExamInfo = JSON.parse(this.responseText);
            if (dataExamInfo) {
                dataExamInfo.forEach(ele=>{
                    $(`#table-${ele.subjectInfo.subjectID.replace(" ","-")}`).hide();
                    $(`#noti-${ele.subjectInfo.subjectID.replace(" ","-")}`).html("Bạn đã đăng ký môn này rồi");
                    $(`.reg[target=${ele.examInfoID}]`).attr("checked",true);
                    $(`#reged-body`).append(`
                        <tr>
                            <td>${ele.subjectInfo.subjectID}</td>
                            <td>${ele.subjectInfo.subjectName}</td>
                            <td>${ele.SBD}</td>
                            <td>${ele.roomInfo.roomNumber}</td>
                            <td>${ele.roomInfo.roomAddress}</td>
                            <td>${ele.shiftInfo.examDay}</td>
                            <td>${ele.shiftInfo.startTime}</td>
                            <td>${ele.subjectInfo.examDuration}</td>
                            <td><button type="button" action="dereg" target="${ele.examInfoID}">X</button></td>
                        </tr>
                    `);
                });
                $('button[action=dereg]').unbind();
                $('button[action=dereg]').on('click',function(){
                    var target = $(this).attr("target");
                    var xhttp = new XMLHttpRequest();
                    xhttp.onloadend = function(){
                        initReged();
                        if(this.status == 200){
                            window.top.$.notify({
                                title: '<strong>Thành công</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Đã hủy" //message
                            },{
                                type: 'info',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                        }else if(this.status == 401){
                            window.top.location.href = "login"
                        }
                        else{
                            window.top.$.notify({
                                title: '<strong>Lỗi</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Không hủy được môn" //message
                            },{
                                type: 'danger',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                        }
                    }
                    xhttp.open("GET","/api/dereg?examInfoID="+target+"&studentID="+studentID,true);
                    xhttp.send();
                });
            }
            else{
                window.top.$.notify({
                    title: '<strong>Lỗi</strong>', //Tên thông báo
                    icon: 'glyphicon glyphicon-star',
                    message: "Không thể lấy danh sách môn đã đăng ký!" //message
                  },{
                    type: 'danger',
                    animate: {
                          enter: 'animated fadeInUp',
                      exit: 'animated fadeOutRight'
                    },
                    placement: {
                      from: "bottom",
                      align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                  });
            }
        }
    }
    getRegistered.open('GET', '/api/get-registered?studentID='+studentID, true);
    getRegistered.send();
}

function cancel() {
    window.top.location.href = "home.html#student_list";
}