function exportHTML(){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("print-area").innerHTML+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'document.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
}

function printNow() {
    var printContents = document.getElementById('print-area').innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

var xml = new XMLHttpRequest();

xml.onloadend = function() {
    if(this.status==200){
        var data = JSON.parse(this.responseText);
        if(data) {
            $('#subjectID').html("<b>" + String(data.subjectID) + "</b>");
            $('#subjectName').html("<b>" + String(data.subjectInfo.subjectName) + "</b>");
            $('#tins').html("<b>" + String(data.subjectInfo.tins) + "</b>");
            $('#examDay').html("<b>" + String(data.shiftInfo.examDay) + "</b>");
            $('#startTime').html("<b>" + String(data.shiftInfo.startTime) + "</b>");
            $('#examDuration').html("<b>" + String(data.subjectInfo.examDuration) + " phút</b>");
            $('#roomNumber').html("<b>" + String(data.roomInfo.roomNumber) + "</b>");
            $('#roomAddress').html("<b>" + String(data.roomInfo.roomAddress) + "</b>");
            $('#slots').html("<b>" + String(data.roomInfo.slots) + "</b>");

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0');
            var yyyy = today.getFullYear();

            $('#today').html("Ngày " + String(dd) + " tháng " + String(mm) + " năm " + String(yyyy));
            $('#placeAndTime').append("ngày " + String(dd) + " tháng " + String(mm) + " năm " + String(yyyy));

            var getRegistered = new XMLHttpRequest();

            getRegistered.onloadend = function() {
                if(this.status==200){
                    var dataExamInfo = JSON.parse(this.responseText);
                    if (dataExamInfo) {
                        $('#numberOfRegister').html(String(dataExamInfo.length));

                        dataExamInfo.forEach((studentInfo, studentInfoIndex, studentInfoArray) => {
                            if(studentInfoIndex === studentInfoArray.length - 1) {
                                $('#examList > tbody > tr').eq(studentInfoIndex).after(`
                                    <tr>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${studentInfoIndex+1}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${studentInfo.studentID}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${studentInfo.lastName} ${studentInfo.firstName}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${studentInfo.dob}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${studentInfo.yearClass}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">${studentInfo.SBD}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center; border-right:1px solid #000;"></th>
                                    </tr>
                                `);    
                            } else {
                                $('#examList > tbody > tr').eq(studentInfoIndex).after(`
                                    <tr>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${studentInfoIndex+1}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${studentInfo.studentID}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${studentInfo.lastName} ${studentInfo.firstName}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${studentInfo.dob}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${studentInfo.yearClass}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center;">${studentInfo.SBD}</th>
                                        <td style="border-top: 1px solid #000; border-left: 1px solid #000; text-align: center; border-right:1px solid #000;"></th>
                                    </tr>
                                `);
                            }
                        })
                    } else {
                        console.log("get-registered return none");
                    }
                }
                else if(this.status==401){
                    window.top.location.href="login";
                }
                else{
                    window.alert("Không thể lấy dữ liệu sinh viên");
                }
            }
            getRegistered.open('GET', '/api/list-student-in-exam-info?examInfoID='+parseURLParams(window.top.location.href).examInfoID[0], true);
            getRegistered.send();
        } else {
            console.log("whoami return none");
        }
    }
    else if(this.status == 401){
        window.top.location.href = "login";
    }
    else{
        window.alert("Không thể lấy dữ liệu của phòng thi");
    }
}
xml.open('GET','/api/get-exam-info-detail?examInfoID='+parseURLParams(window.top.location.href).examInfoID[0],true);
xml.send();

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}