$('document').ready(function(){
    var select_groups = [];
    const option_groups = ['Not assigned','Mã học phần','Giảng viên','Năm học','Học kỳ','Lớp học'];
    let reader = new FileReader();

    //upload file button
    document.querySelector("html").classList.add('js');
    var fileInput  = document.querySelector( ".input-file" ),  
        button     = document.querySelector( ".input-file-trigger" ),
        the_return = document.querySelector(".file-return");
        
    button.addEventListener( "keydown", function( event ) {  
        if ( event.keyCode == 13 || event.keyCode == 32 ) {  
            fileInput.focus();  
        }  
    });
    button.addEventListener( "click", function( event ) {
    fileInput.focus();
    return false;
    });  
    fileInput.addEventListener( "change", function( event ) {  
        the_return.innerHTML = this.value.split("\\")[this.value.split("\\").length-1];  
    }); 

    //Read data from excel file
    reader.onload = (e)=>{
        var data = new Uint8Array(e.target.result);
        var workbook = XLSX.read(data, {dateNF: 'dd"/"mm"/"yyyy', type: 'array'});
        workbook.SheetNames.forEach(ele=>{
            $('#sheets').append(`<option value="${ele}">${ele}</option>`);
        });
        var xlsdata = initData(XLSX.utils.sheet_to_json(workbook.Sheets[$('#sheets').val()], {dateNF: 'dd"/"mm"/"yyyy', defval:"", raw:false}));
        renderData(xlsdata);

        //Change sheet focus
        $('#sheets').on('change',(e)=>{
            xlsdata = initData(XLSX.utils.sheet_to_json(workbook.Sheets[$('#sheets').val()], {dateNF: 'dd"/"mm"/"yyyy',defval:"", raw:false}));
            renderData(xlsdata);
        });

        //Save button click
        $('#save').on('click', (e)=>{
            var selected = {'Mã học phần':'',
            'Giảng viên':'',
            'Năm học':'',
            'Học kỳ':'',
            'Lớp học':''
            };

            // Checking not selected attributes
            select_groups.forEach(ele=>{
                if(ele.value!="Not assigned"){
                    selected[ele.value]=ele.getAttribute('col');
                }
            });
            var notSelected = [];
            Object.keys(selected).forEach(ele=>{
                if(selected[ele].length==0){
                    notSelected.push(ele);
                }
            })

            if(notSelected.length==0){
                sendData(genClassesList(xlsdata,selected));
                //Call API
            }
            else if(notSelected.includes('Mã học phần')||notSelected.includes('Giảng viên')||notSelected.includes('Năm học')||notSelected.includes('Học kỳ')){
                //Can Customize
                window.alert('Giá trị cho [Mã học phần, Mã môn học, Giảng viên, Năm học, Học kỳ] là bắt buộc!');
            }
            else{
                //Can Customize
                if(window.confirm(`${notSelected.length} attribute(s) are not selected.\n[${notSelected.join(', ')}]\nContinue?`)){
                    sendData(genClassesList(xlsdata,selected));
                    //Call API  
                }
            }
        });
    }
    $('#excel-input').on('change',(e)=>{
        file = e.target.files[0];
        reader.readAsArrayBuffer(file);
    });

    // Show excel contents
    function renderData(xlsdata){
        try{
            $('#data-head').html('');
            $('#data').html('');
            $('.right-action-upfile').show();
            
            // Assign attribute selection
            var tr = document.createElement('tr');
            xlsdata.head.forEach((ele)=>{
                var select = document.createElement('select');
                select.setAttribute('col',ele);
                select.classList.add('select-key');
                // Check duplicate selection
                select.addEventListener('change', (e)=>{
                    if(e.target.value!="Not assigned"){
                        e.target.classList.add('assigned');
                        select_groups.forEach(ele=>{
                            if(ele.value == e.target.value && e.target!=ele){
                                ele.value = "Not assigned";
                                ele.classList.remove('assigned');
                            }
                        });
                    }
                    else{
                        e.target.classList.remove('assigned');
                    }
                })
                option_groups.forEach(ele=>{
                    var opt = document.createElement('option');
                    opt.innerHTML = ele;
                    select.append(opt);
                })
                select_groups.push(select);
                var th = document.createElement('th');
                th.append(select);
                tr.append(th);
            });
            tr.append(document.createElement('th'));
            $('#data-head').append(tr);

            // THead key
            tr = document.createElement('tr');
            xlsdata.head.forEach((ele)=>{
                var th = document.createElement('th');
                th.innerHTML = ele;
                tr.append(th);
            })
            tr.append(document.createElement('th'));
            $('#data-head').append(tr);

            //TBody generate
            Object.keys(xlsdata.data).forEach(line=>{
                var tr = document.createElement('tr');
                tr.setAttribute('row', line);
                xlsdata.head.forEach(ele=>{
                    var td = document.createElement('td');
                    td.setAttribute('row',line);
                    td.setAttribute('col',ele);
                    td.innerHTML = (xlsdata.data[line][ele]||"");
                    tr.append(td);
                });
                var del_row_butt = document.createElement('a');
                del_row_butt.classList.add('del-button');
                del_row_butt.setAttribute('href','#');
                del_row_butt.onclick = (e)=>{
                    delete xlsdata.data[line];
                    renderData(xlsdata);
                }
                var td = document.createElement('td');
                td.style.width = "30px";
                td.append(del_row_butt)
                tr.append(td);
                $('#data').append(tr);
            })
        }
        catch(err){
            console.log(err);
        }
    }

    // Initialize data from excel file
    function initData(data){
        var head = [];
        Object.keys(data).forEach(line=>{
            Object.keys(data[line]).forEach(ele=>{
                if(!head.includes(ele)) head.push(ele);
            });
        });
        return {'data': data, 'head': head};
    }

    //Generate Class Model list
    function genClassesList(xlsdata, selected){
        classes_list = [];
        Object.keys(xlsdata.data).forEach(line=>{
            var classs = new Class(xlsdata.data[line][selected['Mã học phần']]||'',
            parseURLParams(window.top.location.href).subjectID[0],
            xlsdata.data[line][selected['Giảng viên']]||'',
            xlsdata.data[line][selected['Năm học']]||'',
            xlsdata.data[line][selected['Học kỳ']]||'',
            xlsdata.data[line][selected['Lớp học']]||'',
            );
            classes_list.push(classs);
        });
        return classes_list;
    }
});

function sendData(data){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            window.alert(`Sucessfully write ${JSON.parse(this.responseText).length} / ${data.length}`);
            window.parent.$("#main-content").fadeIn(200);
            window.parent.$("#excel-container").fadeOut(200);
            window.parent.initData();
        }
        else{
            console.log(this);
        }
    }
    xhttp.open('POST',"/api/add-list-class", true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify(data));
}

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}