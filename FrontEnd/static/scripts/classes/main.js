$('document').ready(function(){
    $('#import').on('click',function(){
        $('#main-content').fadeOut(200);
        $('#excel-container').fadeIn(200);
    });
});

document.getElementById("validate-btn").onclick = function() {
    updateInformation();
}

function updateInformation() {
    if (document.getElementById("classID").value == "" || document.getElementById("teacherName").value == "" || document.getElementById("school-year").value == "" || document.getElementById("term").value == "") {
        $(".alert_text").show();
        return false;
    }

    var subjectID = parseURLParams(window.top.location.href).subjectID;

    var xml = new XMLHttpRequest();
    xml.onloadend = function() {
        window.location.reload();
    }
    xml.open("POST", "/api/add-list-class", true);
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify([{
        "classID": document.getElementById("classID").value,
        "subjectID": subjectID,
        "lecturer": document.getElementById("teacherName").value,
        "schoolYear": document.getElementById("school-year").value,
        "semester": document.getElementById("term").value
    }]));
}

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}