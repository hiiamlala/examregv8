var table;
$('document').ready(function(){
    initData();
})

function render(data){
    if ($.fn.dataTable.isDataTable('#data_table')) {
        table.destroy();
    }
    table = $('#data_table').DataTable({
        data: data,
        columns: [
        {
            title: 'Mã lớp học phần',
            data: 'classID',
        },{
            title: 'Giảng viên',
            data: 'lecturer',
            orderable: false
        },{
            title: 'Niên khóa',
            data: 'schoolYear'
        },{
            title: 'Học kỳ',
            data: 'semester'
        // },{
        //     title: 'Phòng học',
        //     data: 'studyRoom'
        },{
            title: 'Modify',
            render: (data, type, row, meta)=>{
                return `<button type="button" data-toggle="tooltip" title="Xem thêm" data-target="${row.classID}" detail="${row.lecturer + ", Kỳ " + row.semester + " năm " + row.schoolYear}" action="info" class="btn-icon"><span class="info_icon"></span></button>
                        <button type="button" data-toggle="tooltip" title="Sửa" data-target="${row.classID}" action="edit" class="btn-icon"><span class="edit_icon"></span></button>        
                        <button type="button" data-toggle="tooltip" title="Xóa" data-target="${row.classID}" detail="${row.lecturer + ", Kỳ " + row.semester + " năm " + row.schoolYear }" action="remove" class="btn-icon"><span class="delete_icon"></span></button>`
            },
            type: "html",
            orderable: false
        }]
    });
    $('#data_table tbody').unbind();
    $('#data_table tbody').on('click', 'button', function () {
        var target = $(this).attr('data-target');
        switch($(this).attr('action')){
            case "remove":
                if(window.confirm(`Are you sure?\nRoom ${$(this).attr('detail')} will be permanently remove!`)){
                    var xhttp = new XMLHttpRequest();
                    xhttp.onloadend = function(){
                        if(this.status == 204){
                            //noti deleted
                            successNoti("Xóa lớp học phần thành công!");
                            initData();
                        }
                        else if(this.status == 401){
                            window.top.location.href = "login";
                        }
                        else{
                            //noti fail
                            window.top.$.notify({
                                title: '<strong>Thất bại</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Xóa lớp học phần không thành công!" //message
                              },{
                                type: 'danger',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                              });
                            initData();
                        }
                    }
                    xhttp.open('GET',`/api/delete-class?id=${target}`,true);
                    xhttp.send();
                }
                break;
            case "edit":
                $(".content .portlet").hide();
                $(".edit-dialog").show();
                $('#edit-classID').val(target);
                for(let i = 0; i < data.length; i++) {
                    if(data[i].classID == target) {
                        $('#edit-teacherName').val(data[i].lecturer);
                        $('#edit-school-year').val(data[i].schoolYear);
                        $('#edit-term').val(data[i].semester);
                    }
                    break;
                }
                $('#save-edition').click(function() {
                    updateClass(target);
                })
                break;
            case "info":
                window.top.location.href = `home.html?classID=${target}#student-by-class_list`;
                break;
        }
    });
};

function initData(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            var data = JSON.parse(this.responseText);
            render(data);
        }
        else if(this.status == 401){
            window.top.location.href = "login";
        }
        else{
            console.log(this);
        }
    }
    xhttp.open('GET','/api/list-classes?subjectID='+parseURLParams(window.top.location.href).subjectID[0],true);
    xhttp.send();
}

function updateClass(target) {
    var updateRequest = {
        classID: target,
        updateInfo: {
            lecturer: $('#edit-teacherName').val(),
            schoolYear: $('#edit-school-year').val(),
            semester: $('#edit-term').val()
        }
    }

    var xml = new XMLHttpRequest();

    xml.onloadend = function() {
        var data = JSON.parse(this.responseText);
        if(data.success) {
            successNoti("Cập nhật lớp học phần thành công");
            window.location.reload();
        } else {
            failNoti("Cập nhật lớp học phần thất bại");
        }
    }

    xml.open("POST", "api/update-class", true);
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(updateRequest));
}