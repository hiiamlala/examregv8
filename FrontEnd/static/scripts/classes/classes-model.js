class Class{
    constructor(classID,subjectID,lecturer,schoolYear,semester,studyRoom){
        this.classID = classID;
        this.subjectID = subjectID;
        this.lecturer = lecturer;
        this.schoolYear = schoolYear;
        this.semester = semester;
        this.studyRoom = studyRoom;
    }
    toJSON(){
        return{
            classID: this.classID,
            subjectID: this.subjectID,
            lecturer: this.lecturer,
            schoolYear: this.schoolYear,
            semester: this.semester,
            studyRoom: this.studyRoom
        }
    }
}