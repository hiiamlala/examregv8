var table;
$('document').ready(function(){
    initData();
    $('#validate-btn').on('click',function(){
        if($('#MSV').val()!=""){
            var xhttp = new XMLHttpRequest();
            xhttp.onloadend = function(){
                if (this.status == 200){
                    initData();
                    console.log(JSON.parse(this.responseText)[parseURLParams(window.top.location.href).examInfoID[0]]);
                    if(JSON.parse(this.responseText)[parseURLParams(window.top.location.href).examInfoID[0]]){
                        window.top.$.notify({
                            title: '<strong>Thành công</strong>', //Tên thông báo
                            icon: 'glyphicon glyphicon-star',
                            message: "Đăng ký thành công vào ca thi!" //message
                          },{
                            type: 'info',
                            animate: {
                                  enter: 'animated fadeInUp',
                              exit: 'animated fadeOutRight'
                            },
                            placement: {
                              from: "bottom",
                              align: "left"
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 1031,
                          });
                    }
                    else{
                        window.top.$.notify({
                            title: '<strong>Thất bại</strong>', //Tên thông báo
                            icon: 'glyphicon glyphicon-star',
                            message: "Đăng ký không thành công!" //message
                          },{
                            type: 'danger',
                            animate: {
                                  enter: 'animated fadeInUp',
                              exit: 'animated fadeOutRight'
                            },
                            placement: {
                              from: "bottom",
                              align: "left"
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 1031,
                          });
                    }
                }
                else if(this.status == 401){
                    window.top.location.href = "login"
                }
                else{
                    window.top.$.notify({
                        title: '<strong>Thất bại</strong>', //Tên thông báo
                        icon: 'glyphicon glyphicon-star',
                        message: "Đăng ký không thành công!" //message
                      },{
                        type: 'danger',
                        animate: {
                              enter: 'animated fadeInUp',
                          exit: 'animated fadeOutRight'
                        },
                        placement: {
                          from: "bottom",
                          align: "left"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                      });
                }
            }
            xhttp.open("POST","/api/register",true);
            xhttp.setRequestHeader('Content-type','application/json');
            xhttp.send(JSON.stringify({
                studentID: $('#MSV').val(),
                examInfoID: parseURLParams(window.top.location.href).examInfoID[0]
            }));
        }
    });
})

function render(data){
    if ($.fn.dataTable.isDataTable('#data_table')) {
        table.destroy();
    }
    table = $('#data_table').DataTable({
        data: data,
        columns: [{
            title: 'Mã sinh viên',
            data: 'studentID'
        },{
            title: 'Họ và tên',
            render: (data, type, row, meta)=>{
                return row.lastName + " " + row.firstName;
            }
        },{
            title: 'Ngày sinh',
            type: 'date',
            data: 'dob'
        },{
            title: 'Giới tính',
            data: 'sex'
        },{
            title: 'Lớp niên khóa',
            data: 'yearClass'
        },{
            title: 'SBD',
            data: 'SBD',
            type: 'number',
            orderable: true
        },{
            title: 'Modify',
            render: (data, type, row, meta)=>{
                return `<button type="button" data-target="${row.studentID}" action="info" class="btn-icon"><span class="info_icon"></span></button>
                        <button type="button" data-target="${row.studentID}" action="remove" class="btn-icon"><span class="delete_icon"></span></button>`
            },
            type: "html",
            orderable: false
        }]
    });
    $('#data_table tbody').unbind();
    $('#data_table tbody').on('click', 'button', function () {
        var target = $(this).attr('data-target');
        switch($(this).attr('action')){
            case "remove":
                if(window.confirm(`Sinh viên ${$(this).attr('data-target')} sẽ bị xóa khỏi ca thi!`)){
                    var target = $(this).attr("data-target");
                    var xhttp = new XMLHttpRequest();
                    xhttp.onloadend = function(){
                        reged = {};
                        initData();
                        if(this.status == 200){
                            window.top.$.notify({
                                title: '<strong>Thành công</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Đã hủy" //message
                            },{
                                type: 'info',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                        }else if(this.status == 401){
                            window.top.location.href = "login"
                        }
                        else{
                            window.top.$.notify({
                                title: '<strong>Lỗi</strong>', //Tên thông báo
                                icon: 'glyphicon glyphicon-star',
                                message: "Không hủy được môn" //message
                            },{
                                type: 'danger',
                                animate: {
                                      enter: 'animated fadeInUp',
                                  exit: 'animated fadeOutRight'
                                },
                                placement: {
                                  from: "bottom",
                                  align: "left"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                            });
                        }
                    }
                    xhttp.open("GET","/api/dereg?examInfoID="+parseURLParams(window.top.location.href).examInfoID[0]+"&studentID="+target,true);
                    xhttp.send();
                }
                break;
            case "info":
                window.top.location.href = `home.html?studentID=${target}#student-info`;
                break;
        }
    });
};

function initData(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            var data = JSON.parse(this.responseText);
            render(data);
        }
        else if(this.status == 401){
            window.top.location.href = "login";
        }
        else{
            console.log(this);
        }
    }
    xhttp.open('GET','/api/list-student-in-exam-info?examInfoID='+parseURLParams(window.top.location.href).examInfoID[0],true);
    xhttp.send();
}

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}