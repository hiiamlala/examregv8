class Exam_info {
    constructor(subjectID,roomID,shiftID){
        this.subjectID = subjectID,
        this.roomID = roomID,
        this.shiftID = shiftID
    }
    toJSON(){
        return{
            subjectID : this.subjectID,
            roomID : this.roomID,
            shiftID : this.shiftID,
        }
    }
}