var table;
$('document').ready(function(){
    initData();
})

function render(data){
    if ($.fn.dataTable.isDataTable('#data_table')) {
        table.destroy();
    }
    table = $('#data_table').DataTable({
        data: data,
        columns: [
        {
            data: 'examInfoID',
            visible: false
        },{
            title: 'Tên môn học',
            data: 'subjectName',
        },{
            title: 'Thời gian làm bài',
            data: 'examDuration',
            type: 'time'
        },{
            title: 'Ngày thi',
            data: 'examDay',
            type: 'date'
        },{
            title: 'Thời gian thi',
            data: 'startTime',
            type: 'time'
        },{
            title: 'Địa điểm thi',
            data: 'roomAddress',
        },{
            title: 'Phòng',
            data: 'roomNumber',
            orderable: false
        },{
            title: 'Số máy đã đăng ký',
            render: (data, type, row, meta)=>{
                return `${row.used}/${row.slots}`
            },
            type: "html",
            orderable: false
        },{
            title: 'Modify',
            render: (data, type, row, meta)=>{
                return `<button type="button" data-target="${row.examInfoID}" action="info" class="btn-icon"><span class="info_icon"></span></button>`
            },
            type: "html",
            orderable: false
        }]
    });
    $('#data_table tbody').unbind();
    $('#data_table tbody').on('click', 'button', function () {
        var target = $(this).attr('data-target');
        window.top.location.href = `home.html?examInfoID=${target}#exam-student-room`;
    });
};

function initData(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            var data = JSON.parse(this.responseText);
            console.log(data);
            var result = [];
            data.forEach(ele=>{
                result.push({
                    examInfoID: ele.examInfoID,
                    subjectName: ele.subjectInfo.subjectName,
                    examDuration: ele.subjectInfo.examDuration,
                    examDay: ele.shiftInfo.examDay,
                    startTime: ele.shiftInfo.startTime,
                    roomAddress: ele.roomInfo.roomAddress,
                    roomNumber: ele.roomInfo.roomNumber,
                    roomID: ele.roomID,
                    slots: ele.roomInfo.slots,
                    shiftID: ele.shiftID,
                    subjectID: ele.subjectID,
                    used: ele.used
                })
            })
            render(result);
        }
        else if(this.status == 401){
            window.top.location.href = "login";
        }
        else{
            console.log(this);
        }
    }
    xhttp.open('GET','/api/list-exam-info',true);
    xhttp.send();
}