class Class_list{
    constructor(classID,studentID,examCondition){
        this.classID = classID;
        this.studentID = studentID;
        this.examCondition = examCondition||true;
    }
    toJSON(){
        return{
            classID: this.classID,
            studentID: this.studentID,
            examCondition: this.examCondition,
        }
    }
}