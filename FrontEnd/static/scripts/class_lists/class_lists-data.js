var table;
$('document').ready(function(){
    initData();
})

function render(data){
    if ($.fn.dataTable.isDataTable('#data_table')) {
        table.destroy();
    }
    table = $('#data_table').DataTable({
        data: data,
        columns: [
        {
            title: 'class ID',
            data: 'classID',
        },{
            title: 'Mã sinh viên',
            data: 'studentID',
        },{
            title: 'Họ và tên',
            data: 'studentName',
        },{
            title: 'Điều kiện thi',
            data: 'examCondition',
            render: (data, type, row, meta)=>{
                return data==0?"Đủ điều kiện":"Không đủ điều kiện"
            }
        },{
            title: 'Modify',
            render: (data, type, row, meta)=>{
                return `<button type="button" data-target="${row.studentID}" detail="${row}" action="info" class="btn-icon" data-toggle="tooltip" title="Xem thêm"><span class="info_icon"></span></button>
                        <button type="button" data-target="${row.studentID}" action="edit" class="btn-icon" data-toggle="tooltip" title="Chỉnh sửa"><span class="edit_icon"></span></button>        
                        <button type="button" data-target="${row.studentID}" detail="${row}" action="remove" class="btn-icon" data-toggle="tooltip" title="Xóa"><span class="delete_icon"></span></button>`
            },
            type: "html",
            orderable: false
        }]
    });
    $('#data_table tbody').unbind();
    $('#data_table tbody').on('click', 'button', function () {
        var target = $(this).attr('data-target');
        // var detail = $(this).attr('detail').split("|");
        switch($(this).attr('action')){
            case "remove":
                if(window.confirm(`Are you sure?\nRoom ${$(this).attr('detail')} will be permanently remove!`)){
                    var deleteIDs = {
                        studentID: target,
                        classID: parseURLParams(window.top.location.href).classID[0]
                    }
                    var xhttp = new XMLHttpRequest();
                    xhttp.onloadend = function(){
                        if(this.status == 204){
                            successNoti("Xóa sinh viên khỏi lớp thành công");
                            initData();
                        }
                        else if(this.status == 401){
                            window.top.location.href = "login";
                        }
                        else{
                            failNoti("Xóa sinh viên khỏi lớp thất bại");
                        }
                    }
                    xhttp.open('GET',`/api/delete-student-in-class?deleteIDs=${JSON.stringify(deleteIDs)}`,true);
                    xhttp.send();
                }
                break;
            case "edit":
                $(".content .portlet").hide();
                $(".edit-dialog").show();
                $('#edit-MSV').val(target);
                $('#edit-MSV').attr("disabled", true);
                $('#save-edition').click(function() {
                    updateClassList(target);
                })
                break;
            case "info":
                window.top.location.href = `home.html?studentID=${target}#student-info`;
                break;
        }
    });
};

function updateClassList(target) {
    var examCondition = $("#edit-condition").val();
    if(examCondition == "pass-condition") {
        examCondition = false;
    } else {
        examCondition = true;
    }
    var updateInfo = {
        studentID: target,
        classID: parseURLParams(window.top.location.href).classID[0],
        examCondition: examCondition
    }
    var xml = new XMLHttpRequest();

    xml.onloadend = function() {
        var data = JSON.parse(this.responseText);
        if(data.success) {
            successNoti("Cập nhật thành công");
            window.location.reload();
        } else {
            failNoti("Cập nhật thất bại");
        }
    }

    xml.open("POST", "api/update-student-in-class", true);
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(updateInfo));
}

function initData(){
    var xhttp = new XMLHttpRequest();
    xhttp.onloadend = function(){
        if(this.status == 200){
            var data = JSON.parse(this.responseText);
            render(data);
        }
        else if(this.status == 401){
            window.top.location.href = "login";
        }
        else{
            console.log(this);
        }
    }
    xhttp.open('GET','/api/list-class-lists?classID='+parseURLParams(window.top.location.href).classID[0],true);
    xhttp.send();
}

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}