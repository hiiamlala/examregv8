$('document').ready(function(){
    $('#import').on('click',function(){
        $('#main-content').fadeOut(200);
        $('#excel-container').fadeIn(200);
    });
});

$('#validate-btn').click(function() {
    addStudentToClass();
})

function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}

function addStudentToClass() {
    var examCondition = $("#condition").val();
    if(examCondition == "pass-condition") {
        examCondition = false;
    } else {
        examCondition = true;
    }
    var studentInClassInfo = {
        classID: parseURLParams(window.top.location.href).classID[0],
        studentID: $('#MSV').val(),
        examCondition: examCondition
    }

    var xml = new XMLHttpRequest();

    xml.onloadend = function() {
        var data = JSON.parse(this.responseText);
        console.log(data);
        if(data.success) {
            successNoti("Thêm sinh viên vào lớp thành công")
            window.location.reload();
        } else {
            failNoti("Thêm sinh viên vào lớp thất bại");
        }
    }

    xml.open("POST", "api/add-student-to-class", true);
    xml.setRequestHeader("Content-Type", "application/json");
    xml.send(JSON.stringify(studentInClassInfo));
}