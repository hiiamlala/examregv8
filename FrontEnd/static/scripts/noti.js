function successNoti(message) {
    window.top.$.notify({
        title: '<strong>Thành công</strong>', //Tên thông báo
        icon: 'glyphicon glyphicon-star',
        message: `${message}` //message
    },{
        type: 'info',
        animate: {
              enter: 'animated fadeInUp',
          exit: 'animated fadeOutRight'
        },
        placement: {
          from: "bottom",
          align: "left"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
    });
}

function failNoti(message) {
    window.top.$.notify({
        title: '<strong>Lỗi</strong>', //Tên thông báo
        icon: 'glyphicon glyphicon-star',
        message: `${message}` //message
    },{
        type: 'info',
        animate: {
              enter: 'animated fadeInUp',
          exit: 'animated fadeOutRight'
        },
        placement: {
          from: "bottom",
          align: "left"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
    });
}

module.exports = {
    successNoti, failNoti
}