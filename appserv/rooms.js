const DBinterfaces = require("./DBinterfaces")

function showSpecificRoom(ID, callback) {
    DBinterfaces.showSpecificRoom(ID, callback);
}

function listRoomsWithCondition(condition, callback) {
    DBinterfaces.listRoomsWithCondition(condition, callback);
}

function listRooms(callback) {
    DBinterfaces.listRooms(callback);
}

function insertRoom(roomInfo, callback) {
    var successUploadRoom = false;
    var columnsString = "";
    var valuesString = "";
    Object.keys(roomInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + roomInfo[key] + '", ';
    });
    columnsString = columnsString.substring(0, columnsString.length - 2);
    valuesString = valuesString.substring(0, valuesString.length - 2);
    DBinterfaces.insertRoom(columnsString, valuesString, (uploadSuccessfully) => {
        if(uploadSuccessfully) {
            successUploadRoom = true;
            callback(successUploadRoom);
        }
    });
}

function updateRoom(updateInfo, roomID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateRoom(updateString, roomID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteRoom(roomID, callback) {
    DBinterfaces.deleteRoom(roomID, callback);
}

function uploadRoomList(roomList, callback) {
    try {
        var err = {}, res = {};
        var scannedRoom = 0, successUploadRoom = 0, failedUploadRoom = [];
        roomList.forEach(room => {
            var columnsString = "";
            var valuesString = "";
            Object.keys(room).forEach(key => {
                columnsString += key + ", ";
                valuesString += '"' + room[key] + '", ';
            });
            columnsString = columnsString.substring(0, columnsString.length - 2);
            valuesString = valuesString.substring(0, valuesString.length - 2);
            DBinterfaces.insertRoom(columnsString, valuesString, (uploadSuccessfully) => {
                if(uploadSuccessfully) successUploadRoom++;
                else failedUploadRoom.push(room);
                scannedRoom++;
                if(scannedRoom == roomList.length){
                    err.failedUploadRoom = failedUploadRoom;
                    res.successUploadRoom = successUploadRoom;
                    callback(err, res);
                }
            });
        });
    } catch(err) {
        throw err;
    }
}

function updateRoomList(updateRoomList, callback) {
    try {
        var err = {}, res = {};
        var scannedRoom = 0, successUpdate = 0, notAffectedRoom = [], notChangedRoom = [];
        updateRoomList.forEach(room => {
            var updateElement = "";
            var roomID;
            Object.keys(room).forEach(key => {
                if(key == "roomID") {
                    roomID = room[key];
                } else {
                    updateElement += key + "='" + String(room[key]) + "', ";
                }
            })
            updateElement = updateElement.substring(0, updateElement.length - 2);
            DBinterfaces.updateRoom(updateElement, roomID, (updateError, affectedRows, changedRows) => {

                if(updateError) {
                    console.log("Update Room list failed at room: " + String(room));
                }
                else if(affectedRows == 0) {
                    notAffectedRoom.push(room);
                }
                else if(changedRows == 0) {
                    notChangedRoom.push(room);
                }
                else successUpdate++;
                
                scannedRoom++;
                if(scannedRoom == updateRoomList.length) {
                    err.notAffectedRoom = notAffectedRoom;
                    err.notChangedRoom = notChangedRoom;
                    res.successUpdate = successUpdate;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function deleteRoomList(deleteRoomIDList, callback) {
    try {
        var err = {}, res = {};
        var scannedRoom = 0, successDelete = 0, notAffectedRoom = [];
        deleteRoomIDList.forEach(roomID => {
            DBinterfaces.deleteRoom(roomID, (deleteError, affectedRows) => {
                if(deleteError) {
                    console.log("Delete Room list failed at room: " + String(roomID));
                }
                else if(affectedRows == 0) {
                    notAffectedRoom.push(roomID);
                }
                else successDelete++;
                
                scannedRoom++;
                if(scannedRoom == deleteRoomIDList.length) {
                    err.notAffectedRoom = notAffectedRoom;
                    res.successDelete = successDelete;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

module.exports = {
    showSpecificRoom, listRoomsWithCondition, listRooms, insertRoom, updateRoom, deleteRoom,
    uploadRoomList, updateRoomList, deleteRoomList
}