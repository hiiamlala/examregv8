const DBinterfaces = require("./DBinterfaces")
const crypto = require('crypto')

function generateRandomSalt() {
    return crypto.randomBytes(16).toString('hex');
}

function hashPassWithSalt(password, salt) {
    return crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
}

function validatePassword(password, salt, hashPass) {
    var hash = crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
    return hash === hashPass;
}

function generateRandomToken() {
    return crypto.randomBytes(16).toString('hex');
}

function changePassword(changeInfo, callback) {
    showSpecificUser(changeInfo.userID, (userInfo) => {
        var newHashPass = hashPassWithSalt(changeInfo.newPass, userInfo.saltString);
        var newInfo = {
            hashPass: newHashPass
        }
        updateUser(newInfo, userInfo.userID, (updateError, affectedRows, changedRows) => {
            if(updateError || affectedRows == 0 || changedRows == 0) {
                callback(false);
            } else {
                callback(true);
            }
        })
    })
}

function showSpecificUser(ID, callback) {
    DBinterfaces.showSpecificUser(ID, callback);
}

function listUsersWithCondition(condition, callback) {
    DBinterfaces.listUsersWithCondition(condition, callback);
}

function listUsers(callback) {
    DBinterfaces.listUsers(callback);
}

function insertUser(userInfo, callback) {
    var successUploadUser = false;
    var columnsString = "";
    var valuesString = "";
    Object.keys(userInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + userInfo[key] + '", ';
    });
    columnsString = columnsString.substring(0, columnsString.length - 2);
    valuesString = valuesString.substring(0, valuesString.length - 2);
    DBinterfaces.insertUser(columnsString, valuesString, (uploadSuccessfully) => {
        if(uploadSuccessfully) {
            successUploadUser = true;
            callback(successUploadUser);
        }
    });
}

function updateUser(updateInfo, userID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateUser(updateString, userID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteUser(userID, callback) {
    DBinterfaces.deleteUser(userID, callback);
}

function uploadUserList(userList, callback) {
    try {
        var err = {}, res = {};
        var scannedUser = 0, successUploadUser = 0, failedUploadUser = [];
        userList.forEach(user => {
            var columnsString = "";
            var valuesString = "";
            Object.keys(user).forEach(key => {
                columnsString += key + ", ";
                valuesString += '"' + user[key] + '", ';
            });
            columnsString = columnsString.substring(0, columnsString.length - 2);
            valuesString = valuesString.substring(0, valuesString.length - 2);
            DBinterfaces.insertUser(columnsString, valuesString, (uploadSuccessfully) => {
                if(uploadSuccessfully) successUploadUser++;
                else failedUploadUser.push(user);
                scannedUser++;
                if(scannedUser == userList.length){
                    err.failedUploadUser = failedUploadUser;
                    res.successUploadUser = successUploadUser;
                    callback(err, res);
                }
            });
        });
    } catch(err) {
        throw err;
    }
}

function updateUserList(updateUserList, callback) {
    try {
        var err = {}, res = {};
        var scannedUser = 0, successUpdate = 0, notAffectedUser = [], notChangedUser = [];
        updateUserList.forEach(user => {
            var updateElement = "";
            var userID;
            Object.keys(user).forEach(key => {
                if(key == "userID") {
                    userID = user[key];
                } else {
                    updateElement += key + "='" + String(user[key]) + "', ";
                }
            })
            updateElement = updateElement.substring(0, updateElement.length - 2);
            DBinterfaces.updateUser(updateElement, userID, (updateError, affectedRows, changedRows) => {

                if(updateError) {
                    console.log("Update User list failed at user: " + String(user));
                }
                else if(affectedRows == 0) {
                    notAffectedUser.push(user);
                }
                else if(changedRows == 0) {
                    notChangedUser.push(user);
                }
                else successUpdate++;
                
                scannedUser++;
                if(scannedUser == updateUserList.length) {
                    err.notAffectedUser = notAffectedUser;
                    err.notChangedUser = notChangedUser;
                    res.successUpdate = successUpdate;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function deleteUserList(deleteUserIDList, callback) {
    try {
        var err = {}, res = {};
        var scannedUser = 0, successDelete = 0, notAffectedUser = [];
        deleteUserIDList.forEach(userID => {
            DBinterfaces.deleteUser(userID, (deleteError, affectedRows) => {
                if(deleteError) {
                    console.log("Delete User list failed at user: " + String(userID));
                }
                else if(affectedRows == 0) {
                    notAffectedUser.push(userID);
                }
                else successDelete++;
                
                scannedUser++;
                if(scannedUser == deleteUserIDList.length) {
                    err.notAffectedUser = notAffectedUser;
                    res.successDelete = successDelete;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

module.exports = {
    generateRandomSalt, hashPassWithSalt, validatePassword, generateRandomToken, 
    showSpecificUser, listUsersWithCondition, listUsers, insertUser, updateUser, deleteUser,
    uploadUserList, updateUserList, deleteUserList, changePassword
}