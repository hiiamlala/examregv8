const DBinterfaces = require("./DBinterfaces");
const users = require('./users');
const classes = require('./classes');

function showSpecificStudent(ID, callback) {
    DBinterfaces.showSpecificStudent(ID, callback);
}

function listStudentsWithCondition(condition, callback) {
    DBinterfaces.listStudentsWithCondition(condition, callback);
}

function listStudents(callback) {
    DBinterfaces.listStudents(callback);
}

function insertStudent(studentInfo, callback) {
    var res = {};
    var successUploadStudent = false, successCreateStudentUser = false;
    var columnsString = "";
    var valuesString = "";
    var studentID;
    Object.keys(studentInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + studentInfo[key] + '", ';
        if(key == "studentID") studentID = studentInfo[key];
    });
    columnsString = columnsString.substring(0, columnsString.length - 2);
    valuesString = valuesString.substring(0, valuesString.length - 2);
    DBinterfaces.insertStudent(columnsString, valuesString, (uploadSuccessfully) => {
        if(uploadSuccessfully) {
            successUploadStudent = true;
            var salt = users.generateRandomSalt();
            var userInfo = {
                userID: String(studentID),
                hashPass: String(users.hashPassWithSalt(studentID, salt)),
                saltString: String(salt),
                roleType: "student",
                userStatus: "ChangePass"
            }
            users.insertUser(userInfo, (created) => {
                if(created) successCreateStudentUser = true;
                res.successUploadStudent = successUploadStudent;
                res.successCreateStudentUser = successCreateStudentUser;
                callback(res);
            })
        }
    });
}

function updateStudent(updateInfo, studentID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateStudent(updateString, studentID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteStudent(studentID, callback) {
    DBinterfaces.deleteStudent(studentID, callback);
}

function listStudentsJoinSubjectWithSubjectID(subjectID, callback) {
    var resultStudentList = [];
    classes.listClassesWithSubjectID(subjectID, (resultClassList) => {
        if(resultClassList.length==0){
            callback(resultStudentList);
        }
        resultClassList.forEach((classInfo, classInfoIndex, classInfoArray) => {
            DBinterfaces.listStudentsInClassesWithCondition("classID='" + String(classInfo.classID) + "'", (resultStudentsInClassList) => {
                if((classInfoIndex === classInfoArray.length - 1) && resultStudentsInClassList.length == 0){
                    callback(resultStudentList);
                }
                resultStudentsInClassList.forEach((studentInClass, studentInClassIndex, studentInClassArray) => {
                    showSpecificStudent(studentInClass.studentID, (studentInfo) => {
                        resultStudentList.push(studentInfo);
                        if(classInfoIndex === classInfoArray.length - 1 && studentInClassIndex === studentInClassArray.length - 1) {
                            callback(resultStudentList);
                        }
                    })
                })
            })
        })
    })
}

function uploadStudentList(studentList, callback) {
    try {
        var err = {}, res = {};
        var scannedStudent = 0, successUploadStudent = 0, failedUploadStudent = [], successCreateStudentUser = 0, failedCreateStudentUserID = [];
        studentList.forEach(student => {
            var columnsString = "";
            var valuesString = "";
            var studentID;
            Object.keys(student).forEach(key => {
                columnsString += key + ", ";
                valuesString += '"' + student[key] + '", ';
                if(key == "studentID") studentID = student[key];
            });
            columnsString = columnsString.substring(0, columnsString.length - 2);
            valuesString = valuesString.substring(0, valuesString.length - 2);
            DBinterfaces.insertStudent(columnsString, valuesString, (uploadSuccessfully) => {
                if(uploadSuccessfully) {
                    successUploadStudent++;
                    var salt = users.generateRandomSalt();
                    var userInfo = {
                        userID: String(studentID),
                        hashPass: String(users.hashPassWithSalt(studentID, salt)),
                        saltString: String(salt),
                        roleType: "student",
                        userStatus: "ChangePass"
                    }
                    users.insertUser(userInfo, (created) => {
                        if(created) successCreateStudentUser++;
                        else failedCreateStudentUserID.push(studentID);
                    })
                }
                else failedUploadStudent.push(student);
                scannedStudent++;
                if(scannedStudent == studentList.length) {
                    err.failedUploadStudent = failedUploadStudent;
                    err.failedCreateStudentUserID = failedCreateStudentUserID;
                    res.successUploadStudent = successUploadStudent;
                    res.successCreateStudentUser = successCreateStudentUser;
                    callback(err, res);
                }
            });
        });
    } catch(err) {
        throw err;
    }
}

function updateStudentList(updateStudentList, callback) {
    try {
        var err = {}, res = {};
        var scannedStudent = 0, successUpdate = 0, notAffectedStudent = [], notChangedStudent = [];
        updateStudentList.forEach(student => {
            var updateElement = "";
            var studentID;
            Object.keys(student).forEach(key => {
                if(key == "studentID") {
                    studentID = student[key];
                } else {
                    updateElement += key + "='" + String(student[key]) + "', ";
                }
            })
            updateElement = updateElement.substring(0, updateElement.length - 2);
            DBinterfaces.updateStudent(updateElement, studentID, (updateError, affectedRows, changedRows) => {
                
                if(updateError) {
                    console.log("Update Student list failed at student: " + String(student));
                }
                else if(affectedRows == 0) {
                    notAffectedStudent.push(student);
                }
                else if(changedRows == 0) {
                    notChangedStudent.push(student);
                }
                else successUpdate++;

                scannedStudent++;
                if(scannedStudent == updateStudentList.length) {
                    err.notAffectedStudent = notAffectedStudent;
                    err.notChangedStudent = notChangedStudent;
                    res.successUpdate = successUpdate;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function deleteStudentList(deleteStudentIDList, callback) {
    try {
        var err = {}, res = {};
        var scannedStudent = 0, successDelete = 0, notAffectedStudent = [], successDeletedStudentUser = 0, failedDeletedStudentUserID = [];
        deleteStudentIDList.forEach(studentID => {
            DBinterfaces.deleteStudent(studentID, (deleteError, affectedRows) => {
                if(deleteError) {
                    console.log("Delete student list failed at student: " + String(studentID));
                }
                else if(affectedRows == 1) {
                    successDelete++;
                    DBinterfaces.deleteUser(studentID, (deleteUserError, affectedUserRows) => {
                        if(deleteUserError) {
                            console.log("Delete student user list failed at student: " + String(studentID));
                        }
                        else if(affectedUserRows == 1) {
                            successDeletedStudentUser++;
                        }
                        else failedDeletedStudentUserID.push(studentID);
                    })
                }
                else notAffectedStudent.push(studentID);
                scannedStudent++;
                if(scannedStudent == deleteStudentIDList.length) {
                    err.notAffectedStudent = notAffectedStudent;
                    err.failedDeletedStudentUserID = failedDeletedStudentUserID;
                    res.successDelete = successDelete;
                    res.successDeletedStudentUser = successDeletedStudentUser;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

module.exports = {
    showSpecificStudent, listStudentsWithCondition, listStudents, insertStudent, updateStudent, deleteStudent,
    uploadStudentList, updateStudentList, deleteStudentList,
    listStudentsJoinSubjectWithSubjectID
}