const DBinterfaces = require("./DBinterfaces");
// const examInfos = require("./examInfos");
const rooms = require("./rooms");

function showSpecificExamList(studentID, examInfoID, callback) {
    DBinterfaces.showSpecificStudentInExam(studentID, examInfoID, callback);
}

function listStudentsInExamsWithCondition(condition, callback) {
    DBinterfaces.listStudentsInExamsWithCondition(condition, callback);
}

function listStudentsInExams(callback) {
    DBinterfaces.listExamList(callback);
}

function insertExamList(studentInExamInfo, callback) {
    listStudentsInExamsWithCondition("examInfoID='" + String(studentInExamInfo.examInfoID) + "'", (resultStudentInExamList) => {
        require("./examInfos").showSpecificExamInfo(studentInExamInfo.examInfoID, (examInfo) => {
            rooms.showSpecificRoom(examInfo.roomID, (roomInfo) => {
                var successUploadStudentInExam = false;
                if(resultStudentInExamList.length < roomInfo.slots) {
                    var columnsString = "";
                    var valuesString = "";
                    Object.keys(studentInExamInfo).forEach(key => {
                        columnsString += key + ", ";
                        valuesString += '"' + studentInExamInfo[key] + '", ';
                    });
                    columnsString = columnsString.substring(0, columnsString.length - 2);
                    valuesString = valuesString.substring(0, valuesString.length - 2);
                    DBinterfaces.insertExamList(columnsString, valuesString, (uploadSuccessfully) => {
                        if(uploadSuccessfully) {
                            successUploadStudentInExam = true;
                            callback(successUploadStudentInExam);
                        }
                        else{
                            callback(successUploadStudentInExam);
                        }
                    });
                } else {
                    callback(successUploadStudentInExam);
                }
            })
        })
    })
    
}

function updateExamList(updateInfo, examInfoID, studentID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateExamList(updateString, examInfoID, studentID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteExamList(examInfoID, studentID, callback) {
    DBinterfaces.deleteExamList(examInfoID, studentID, callback);
}

module.exports = {
    showSpecificExamList, listStudentsInExamsWithCondition, listStudentsInExams, insertExamList, updateExamList, deleteExamList
}