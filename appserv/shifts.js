const DBinterfaces = require("./DBinterfaces");
const examLists = require("./examLists");
const examInfos = require("./examInfos");

function showSpecificShift(ID, callback) {
    DBinterfaces.showSpecificShift(ID, callback);
}

function listShiftsWithCondition(condition, callback) {
    DBinterfaces.listShiftsWithCondition(condition, callback);
}

function listShiftUsedByRoomID(roomID, callback) {
    var resultShiftList = [];
    examInfos.listExamInfosWithCondition("roomID='" + String(roomID) + "'", (resultExamInfoList) => {
        resultExamInfoList.forEach((examInfo, examInfoIndex, examInfoArray) => {
            showSpecificShift(examInfo.shiftID, (shiftInfo) => {
                resultShiftList.push(shiftInfo);
                if(examInfoIndex === examInfoArray.length - 1) {
                    callback(resultShiftList);
                }
            })
        }) 
    })
}

function listShifts(callback) {
    DBinterfaces.listShifts(callback);
}

function insertShift(shiftInfo, callback) {
    var successUploadShift = false;
    var columnsString = "";
    var valuesString = "";
    Object.keys(shiftInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + shiftInfo[key] + '", ';
    });
    columnsString = columnsString.substring(0, columnsString.length - 2);
    valuesString = valuesString.substring(0, valuesString.length - 2);
    DBinterfaces.insertShift(columnsString, valuesString, (uploadSuccessfully) => {
        if(uploadSuccessfully) {
            successUploadShift = true;
            callback(successUploadShift);
        }
    });
}

function updateShift(updateInfo, shiftID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateShift(updateString, shiftID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteShift(shiftID, callback) {
    DBinterfaces.deleteShift(shiftID, callback);
}

function listShiftsRegisteredByStudentID(studentID, callback) {
    var resultShiftList = [];
    examLists.listStudentsInExamsWithCondition("studentID='" + String(studentID) + "'", (resultStudentInExamLists) => {
        resultStudentInExamLists.forEach((studentInExam, studentInExamIndex, studentInExamArray) => {
            examInfos.showSpecificExamInfo(studentInExam.examInfoID, (examInfo) => {
                showSpecificShift(examInfo.shiftID, (shiftInfo) => {
                    resultShiftList.push(shiftInfo);
                    if(studentInExamIndex === studentInExamArray.length - 1) {
                        callback(resultShiftList);
                    }
                })
            })
        })
    })
}

function uploadShiftList(shiftList, callback) {
    try {
        var err = {}, res = {};
        var scannedShift = 0, successUploadShift = 0, failedUploadShift = [];
        shiftList.forEach(shift => {
            var columnsString = "";
            var valuesString = "";
            Object.keys(shift).forEach(key => {
                columnsString += key + ", ";
                valuesString += '"' + shift[key] + '", ';
            });
            columnsString = columnsString.substring(0, columnsString.length - 2);
            valuesString = valuesString.substring(0, valuesString.length - 2);
            DBinterfaces.insertShift(columnsString, valuesString, (uploadSuccessfully) => {
                if(uploadSuccessfully) successUploadShift++;
                else failedUploadShift.push(shift);
                scannedShift++;
                if(scannedShift == shiftList.length){
                    err.failedUploadShift = failedUploadShift;
                    res.successUploadShift = successUploadShift;
                    callback(err, res);
                }
            });
        });
    } catch(err) {
        throw err;
    }
}

function updateShiftList(updateShiftList, callback) {
    try {
        var err = {}, res = {};
        var scannedShift = 0, successUpdate = 0, notAffectedShift = [], notChangedShift = [];
        updateShiftList.forEach(shift => {
            var updateElement = "";
            var shiftID;
            Object.keys(shift).forEach(key => {
                if(key == "shiftID") {
                    shiftID = shift[key];
                } else {
                    updateElement += key + "='" + String(shift[key]) + "', ";
                }
            })
            updateElement = updateElement.substring(0, updateElement.length - 2);
            DBinterfaces.updateShift(updateElement, shiftID, (updateError, affectedRows, changedRows) => {

                if(updateError) {
                    console.log("Update Shift list failed at shift: " + String(shift));
                }
                else if(affectedRows == 0) {
                    notAffectedShift.push(shift);
                }
                else if(changedRows == 0) {
                    notChangedShift.push(shift);
                }
                else successUpdate++;
                
                scannedShift++;
                if(scannedShift == updateShiftList.length) {
                    err.notAffectedShift = notAffectedShift;
                    err.notChangedShift = notChangedShift;
                    res.successUpdate = successUpdate;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function deleteShiftList(deleteShiftIDList, callback) {
    try {
        var err = {}, res = {};
        var scannedShift = 0, successDelete = 0, notAffectedShift = [];
        deleteShiftIDList.forEach(shiftID => {
            DBinterfaces.deleteShift(shiftID, (deleteError, affectedRows) => {
                if(deleteError) {
                    console.log("Delete Shift list failed at shift: " + String(shiftID));
                }
                else if(affectedRows == 0) {
                    notAffectedShift.push(shiftID);
                }
                else successDelete++;
                
                scannedShift++;
                if(scannedShift == deleteShiftIDList.length) {
                    err.notAffectedShift = notAffectedShift;
                    res.successDelete = successDelete;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

module.exports = {
    showSpecificShift, listShiftsWithCondition, listShifts, insertShift, updateShift, deleteShift,
    uploadShiftList, updateShiftList, deleteShiftList,
    listShiftsRegisteredByStudentID, listShiftUsedByRoomID
}