const DBinterfaces = require("./DBinterfaces")
const classes = require("./classes");

function showSpecificStudentInClass(studentID, classID, callback) {
    DBinterfaces.showSpecificStudentInClass(studentID, classID, callback);
}

function listStudentsInClassesWithCondition(condition, callback) {
    DBinterfaces.listStudentsInClassesWithCondition(condition, callback);
}

function listStudentsInClassID(classID, callback) {
    listStudentsInClassesWithCondition("classID='" + String(classID) + "'", callback);
}

function listStudentsInClasses(callback) {
    DBinterfaces.listClassList(callback);
}

function insertStudentInClass(studentInClassInfo, callback) {
    var successUploadStudentInClass = false;
    var columnsString = "";
    var valuesString = "";
    Object.keys(studentInClassInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + studentInClassInfo[key] + '", ';
    });
    getNewSBD(String(studentInClassInfo.classID), (SBD) => {
        columnsString += "SBD";
        valuesString += String(SBD);
        
        DBinterfaces.insertClassList(columnsString, valuesString, (uploadSuccessfully) => {
            if(uploadSuccessfully) {
                successUploadStudentInClass = true;
                callback(successUploadStudentInClass);
            }
        });
    })
}

function getNewSBD(classID, callback) {
    var SBD = 1;
    classes.showSpecificClass(classID, (classInfo) => {
        classes.listClassesWithCondition("subjectID='" + String(classInfo.subjectID) + "'", (resultClassList) => {
            resultClassList.forEach((resultClassInfo, resultClassInfoIndex, resultClassInfoArray) => {
                listStudentsInClassesWithCondition("classID='" + String(resultClassInfo.classID) + "'", (resultStudentInClassList) => {
                    SBD += parseInt(resultStudentInClassList.length);
                    if(resultClassInfoIndex === resultClassInfoArray.length - 1) {
                        callback(SBD);
                    }
                })
            })
        })
    })
}

function updateStudentInClass(updateInfo, classID, studentID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "=" + updateInfo[updateElementKey];
            DBinterfaces.updateClassList(updateString, classID, studentID, callback);
        } else {
            updateString += updateElementKey + "=" + updateInfo[updateElementKey] + ",";
        }
    })
}

function deleteStudentInClass(classID, studentID, callback) {
    DBinterfaces.deleteClassList(classID, studentID, callback);
}

function uploadStudentInClassList(studentInClassList, callback) {
    try {
        var err = {}, res = {};
        var scannedStudentInClass = 0, successUploadStudentInClass = 0, failedUploadStudentInClass = [];
        studentInClassList.forEach(studentInClass => {
            var columnsString = "";
            var valuesString = "";
            Object.keys(studentInClass).forEach(key => {
                columnsString += key + ", ";
                valuesString += '"' + studentInClass[key] + '", ';
            });
            getNewSBD(String(studentInClass.classID), (SBD) => {
                columnsString += "SBD";
                valuesString += String(SBD);

                DBinterfaces.insertClassList(columnsString, valuesString, (uploadSuccessfully) => {
                    if(uploadSuccessfully) successUploadStudentInClass++;
                    else failedUploadStudentInClass.push(studentInClass);
                    scannedStudentInClass++;
                    if(scannedStudentInClass == studentInClassList.length){
                        err.failedUploadStudentInClass = failedUploadStudentInClass;
                        res.successUploadStudentInClass = successUploadStudentInClass;
                        callback(err, res);
                    }
                });
            });
        });
    } catch(err) {
        throw err;
    }
}

function updateStudentInClassList(updateStudentInClassList, callback) {
    try {
        var err = {}, res = {};
        var scannedStudentInClass = 0, successUpdate = 0, notAffectedStudentInClass = [], notChangedStudentInClass = [];
        updateStudentInClassList.forEach(studentInClass => {
            var updateElement = "";
            var studentInClassID;
            Object.keys(studentInClass).forEach(key => {
                if(key == "studentInClassID") {
                    studentInClassID = studentInClass[key];
                } else {
                    updateElement += key + "='" + String(studentInClass[key]) + "', ";
                }
            })
            updateElement = updateElement.substring(0, updateElement.length - 2);
            DBinterfaces.updateClassList(updateElement, studentInClassID, (updateError, affectedRows, changedRows) => {

                if(updateError) {
                    console.log("Update StudentInClass list failed at studentInClass: " + String(studentInClass));
                }
                else if(affectedRows == 0) {
                    notAffectedStudentInClass.push(studentInClass);
                }
                else if(changedRows == 0) {
                    notChangedStudentInClass.push(studentInClass);
                }
                else successUpdate++;
                
                scannedStudentInClass++;
                if(scannedStudentInClass == updateStudentInClassList.length) {
                    err.notAffectedStudentInClass = notAffectedStudentInClass;
                    err.notChangedStudentInClass = notChangedStudentInClass;
                    res.successUpdate = successUpdate;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function deleteStudentInClassList(deleteStudentInClassIDList, callback) {
    try {
        var err = {}, res = {};
        var scannedStudentInClass = 0, successDelete = 0, notAffectedStudentInClass = [];
        deleteStudentInClassIDList.forEach(studentInClassID => {
            DBinterfaces.deleteClassList(studentInClassID, (deleteError, affectedRows) => {
                if(deleteError) {
                    console.log("Delete StudentInClass list failed at studentInClass: " + String(studentInClassID));
                }
                else if(affectedRows == 0) {
                    notAffectedStudentInClass.push(studentInClassID);
                }
                else successDelete++;
                
                scannedStudentInClass++;
                if(scannedStudentInClass == deleteStudentInClassIDList.length) {
                    err.notAffectedStudentInClass = notAffectedStudentInClass;
                    res.successDelete = successDelete;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function showForbidenedStudentList(callback, classID=null) {
    var condition = "examCondition=FALSE";
    if(classID != null) {
        condition += " AND classID='" + String(classID) + "'";
    }
    DBinterfaces.listStudentsInClassesWithCondition(condition, (result) => {
        callback(result);
    })
}

function getSBDByStudentIDAndSubjectID(studentID, subjectID, callback) {
    classes.listClassesWithSubjectID(subjectID, (resultClassList) => {
        for(let i = 0; i < resultClassList.length; i++) {
            var found = false;
            showSpecificStudentInClass(studentID, resultClassList[i].classID, (studentInClass) => {
                if(studentInClass) {
                    found = true;
                    callback(studentInClass.SBD);
                }
            })
            if(found) break;
        }
    })
}

module.exports = {
    showSpecificStudentInClass, listStudentsInClassesWithCondition, listStudentsInClasses, insertStudentInClass, updateStudentInClass, deleteStudentInClass,
    uploadStudentInClassList, updateStudentInClassList, deleteStudentInClassList, 
    showForbidenedStudentList, listStudentsInClassID, getSBDByStudentIDAndSubjectID
}