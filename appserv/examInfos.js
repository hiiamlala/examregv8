const DBinterfaces = require("./DBinterfaces");
const subjects = require("./subjects");
// const shifts = require("./shifts");
const examLists = require("./examLists");
const rooms = require("./rooms");
const classLists = require("./classList");
const classes = require("./classes");

function showSpecificExamInfo(ID, callback) {
    DBinterfaces.showSpecificExamInfo(ID, callback);
}

function listExamInfosWithCondition(condition, callback) {
    DBinterfaces.listExamInfosWithCondition(condition, callback);
}

function listExamInfos(callback) {
    DBinterfaces.listExamInfo(callback);
}

function listExamInfosRegisteredByStudentID(studentID, callback) {
    var resultExamInfoList = [];
    examLists.listStudentsInExamsWithCondition("studentID='" + String(studentID) + "'", (resultStudentInExamList) => {
        if(resultStudentInExamList.length == 0){
            callback([]);
        }
        else{
            resultStudentInExamList.forEach((studentInExam, studentInExamIndex, studentInExamArray) => {
                showSpecificExamInfo(studentInExam.examInfoID, (examInfo) => {
                    subjects.showSpecificSubject(examInfo.subjectID, (subjectInfo) => {
                        rooms.showSpecificRoom(examInfo.roomID, (roomInfo) => {
                            require("./shifts").showSpecificShift(examInfo.shiftID, (shiftInfo) => {
                                classes.listClassesWithCondition("subjectID='" + String(subjectInfo.subjectID) + "'", (resultClassList) => {
                                    resultClassList.forEach((classInfo, classInfoIndex, classInfoArray) => {
                                        classLists.showSpecificStudentInClass(studentID, classInfo.classID, (studentInClassInfo) => {
                                            if(studentInClassInfo) {
                                                resultExamInfoList.push({
                                                    examInfoID: examInfo.examInfoID,
                                                    SBD: studentInClassInfo.SBD,
                                                    subjectInfo: subjectInfo,
                                                    roomInfo: roomInfo,
                                                    shiftInfo: shiftInfo
                                                });
                                                if(studentInExamIndex === studentInExamArray.length - 1) {
                                                    callback(resultExamInfoList);
                                                }
                                            }
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        }
    })
}

function isExamInfoAvailable(considerExamInfo, callback) {
    var result = {
        available: false
    };
    var done = false;
    listExamInfosWithCondition("roomID='" + String(considerExamInfo.roomID) + "'", (resultExamInfoList) => {
        if(resultExamInfoList.length){
            resultExamInfoList.forEach((examInfo, examInfoIndex, examInfoArray) => {
                if(!done){
                    subjects.showSpecificSubject(examInfo.subjectID, (subjectInfo) => {
                        subjects.showSpecificSubject(considerExamInfo.subjectID, (considerSubjectInfo) => {
                            require("./shifts").showSpecificShift(examInfo.shiftID, (shiftInfo) => {
                                var timeA = parseInt(considerSubjectInfo.examDuration), startA = considerExamInfo.startTime, dateA = considerExamInfo.examDay;
                                var timeB = parseInt(subjectInfo.examDuration), startB = shiftInfo.startTime, dateB = shiftInfo.examDay;
                                var startSplitA = startA.split(":");
                                startA = parseInt(startSplitA[0])*60 + parseInt(startSplitA[1]);
                                var endA = startA + timeA;
                                var startSplitB = startB.split(":");
                                startB = parseInt(startSplitB[0])*60 + parseInt(startSplitB[1]);
                                var endB = startB + timeB;
                                dateA = dateA.split("/");
                                dateB = dateB.split("/");
                                if(considerExamInfo.shiftID != examInfo.shiftID) {
                                    if(dateA[0] == dateB[0] && dateA[1] == dateB[1] && dateA[2] == dateB[2]) {
                                        if(endA < startB || startA > endB) {
                                            if(examInfoIndex === examInfoArray.length - 1) {
                                                result = {
                                                    available: true
                                                }
                                                if(!done) {
                                                    done = true;
                                                    return callback(result);
                                                }
                                            }
                                        } else {
                                            result.cause = `Time overlap with examInfo ${examInfo.examInfoID} (Start: ${shiftInfo.startTime}, Time: ${subjectInfo.examDuration})`;
                                            result.examInfoID = examInfo.examInfoID;
                                            result.subjectInfo = subjectInfo;
                                            result.shiftInfo = shiftInfo;
                                            if(!done) {
                                                done = true;
                                                return callback(result);
                                            }
                                        }
                                    } else {
                                        if(examInfoIndex === examInfoArray.length - 1) {
                                            result = {
                                                available: true
                                            }
                                            if(!done) {
                                                done = true;
                                                return callback(result);
                                            }
                                        }
                                    }
                                } else {
                                    result.cause = `ShiftID ${examInfo.shiftID} duplicate`;
                                    if(!done) {
                                        done = true;
                                        return callback(result);
                                    }
                                }
                            })
                        })
                    })
                }
            })
        }
        else{
            result = {
                available: true
            }
            if(!done) return callback(result);
        }
    })
}

function insertExamInfo(examInfo, callback) {
    var successUploadExamInfo = false;
    var columnsString = "";
    var valuesString = "";
    Object.keys(examInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + examInfo[key] + '", ';
    });
    columnsString = columnsString.substring(0, columnsString.length - 2);
    valuesString = valuesString.substring(0, valuesString.length - 2);
    DBinterfaces.insertExamInfo(columnsString, valuesString, (uploadSuccessfully) => {
        if(uploadSuccessfully) {
            successUploadExamInfo = true;
            callback(successUploadExamInfo);
        }
    });
}

function updateExamInfo(updateInfo, examInfoID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateExamInfo(updateString, examInfoID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteExamInfo(examInfoID, callback) {
    DBinterfaces.deleteExamInfo(examInfoID, callback);
}

module.exports = {
    showSpecificExamInfo, listExamInfosWithCondition, listExamInfos, insertExamInfo, updateExamInfo, deleteExamInfo,
    isExamInfoAvailable, listExamInfosRegisteredByStudentID
}