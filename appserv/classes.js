const DBinterfaces = require("./DBinterfaces")

function showSpecificClass(ID, callback) {
    DBinterfaces.showSpecificClass(ID, callback);
}

function listClassesWithCondition(condition, callback) {
    DBinterfaces.listClassesWithCondition(condition, callback);
}

function listClasses(callback) {
    DBinterfaces.listClasses(callback);
}

function insertClass(classInfo, callback) {
    var successUploadClass = false;
    var columnsString = "";
    var valuesString = "";
    Object.keys(classInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + classInfo[key] + '", ';
    });
    columnsString = columnsString.substring(0, columnsString.length - 2);
    valuesString = valuesString.substring(0, valuesString.length - 2);
    DBinterfaces.insertClass(columnsString, valuesString, (uploadSuccessfully) => {
        if(uploadSuccessfully) {
            successUploadClass = true;
            callback(successUploadClass);
        }
    });
}

function updateClass(updateInfo, classID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateClass(updateString, classID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteClass(classID, callback) {
    DBinterfaces.deleteClass(classID, callback);
}

function listClassesWithSubjectID(subjectID, callback) {
    DBinterfaces.listClassesWithCondition("subjectID='" + String(subjectID) + "'", callback);
}

function uploadClassList(classList, callback) {
    try {
        var err = {}, res = {};
        var scannedClass = 0, successUploadClass = 0, failedUploadClass = [];
        classList.forEach(class1 => {
            var columnsString = "";
            var valuesString = "";
            Object.keys(class1).forEach(key => {
                columnsString += key + ", ";
                valuesString += '"' + class1[key] + '", ';
            });
            columnsString = columnsString.substring(0, columnsString.length - 2);
            valuesString = valuesString.substring(0, valuesString.length - 2);
            DBinterfaces.insertClass(columnsString, valuesString, (uploadSuccessfully) => {
                if(uploadSuccessfully) successUploadClass++;
                else failedUploadClass.push(class1);
                scannedClass++;
                if(scannedClass == classList.length){
                    err.failedUploadClass = failedUploadClass;
                    res.successUploadClass = successUploadClass;
                    callback(err, res);
                }
            });
        });
    } catch(err) {
        throw err;
    }
}

function updateClassList(updateClassList, callback) {
    try {
        var err = {}, res = {};
        var scannedClass = 0, successUpdate = 0, notAffectedClass = [], notChangedClass = [];
        updateClassList.forEach(class1 => {
            var updateElement = "";
            var classID;
            Object.keys(class1).forEach(key => {
                if(key == "classID") {
                    classID = class1[key];
                } else {
                    updateElement += key + "='" + String(class1[key]) + "', ";
                }
            })
            updateElement = updateElement.substring(0, updateElement.length - 2);
            DBinterfaces.updateClass(updateElement, classID, (updateError, affectedRows, changedRows) => {

                if(updateError) {
                    console.log("Update Class list failed at class: " + String(class1));
                }
                else if(affectedRows == 0) {
                    notAffectedClass.push(class1);
                }
                else if(changedRows == 0) {
                    notChangedClass.push(class1);
                }
                else successUpdate++;
                
                scannedClass++;
                if(scannedClass == updateClassList.length) {
                    err.notAffectedClass = notAffectedClass;
                    err.notChangedClass = notChangedClass;
                    res.successUpdate = successUpdate;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function deleteClassList(deleteClassIDList, callback) {
    try {
        var err = {}, res = {};
        var scannedClass = 0, successDelete = 0, notAffectedClass = [];
        deleteClassIDList.forEach(classID => {
            DBinterfaces.deleteClass(classID, (deleteError, affectedRows) => {
                if(deleteError) {
                    console.log("Delete Class list failed at class: " + String(classID));
                }
                else if(affectedRows == 0) {
                    notAffectedClass.push(classID);
                }
                else successDelete++;
                
                scannedClass++;
                if(scannedClass == deleteClassIDList.length) {
                    err.notAffectedClass = notAffectedClass;
                    res.successDelete = successDelete;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

module.exports = {
    showSpecificClass, listClassesWithCondition, listClasses, insertClass, updateClass, deleteClass,
    uploadClassList, updateClassList, deleteClassList,
    listClassesWithSubjectID
}