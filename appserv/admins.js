const DBinterfaces = require("./DBinterfaces")
const users = require("./users");

function showSpecificAdmin(ID, callback) {
    DBinterfaces.showSpecificAdmin(ID, callback);
}

function listAdminsWithCondition(condition, callback) {
    DBinterfaces.listAdminsWithCondition(condition, callback);
}

function listAdmins(callback) {
    DBinterfaces.listAdmins(callback);
}

function insertAdmin(adminInfo, callback) {
    var res = {};
    var successUploadAdmin = false, successCreateAdminUser = false;
    var columnsString = "";
    var valuesString = "";
    var adminID;
    Object.keys(adminInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + adminInfo[key] + '", ';
        if(key == "adminID") adminID = adminInfo[key];
    });
    columnsString = columnsString.substring(0, columnsString.length - 2);
    valuesString = valuesString.substring(0, valuesString.length - 2);
    DBinterfaces.insertAdmin(columnsString, valuesString, (uploadSuccessfully) => {
        if(uploadSuccessfully) {
            successUploadAdmin = true;
            var salt = users.generateRandomSalt();
            var userInfo = {
                userID: String(adminID),
                hashPass: String(users.hashPassWithSalt(adminID, salt)),
                saltString: String(salt),
                roleType: "admin",
                userStatus: "ChangePass"
            }
            users.insertUser(userInfo, (created) => {
                if(created) successCreateAdminUser = true;
                res.successUploadAdmin = successUploadAdmin;
                res.successCreateAdminUser = successCreateAdminUser;
                callback(res);
            })
        }
    });
}

function updateAdmin(updateInfo, adminID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateAdmin(updateString, adminID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteAdmin(adminID, callback) {
    DBinterfaces.deleteAdmin(adminID, callback);
}

function uploadAdminList(adminList, callback) {
    try {
        var err = {}, res = {};
        var scannedAdmin = 0, successUploadAdmin = 0, failedUploadAdmin = [], successCreateAdminUser = 0, failedCreateAdminUserID = [];
        adminList.forEach(admin => {
            var columnsString = "";
            var valuesString = "";
            var adminID;
            Object.keys(admin).forEach(key => {
                columnsString += key + ", ";
                valuesString += '"' + admin[key] + '", ';
                if(key == "adminID") adminID = admin[key];
            });
            columnsString = columnsString.substring(0, columnsString.length - 2);
            valuesString = valuesString.substring(0, valuesString.length - 2);
            DBinterfaces.insertAdmin(columnsString, valuesString, (uploadSuccessfully) => {
                if(uploadSuccessfully) {
                    successUploadAdmin++;
                    var salt = users.generateRandomSalt();
                    var userInfo = {
                        userID: String(adminID),
                        hashPass: String(users.hashPassWithSalt(adminID, salt)),
                        saltString: String(salt),
                        roleType: "admin",
                        userStatus: "ChangePass"
                    }
                    users.insertUser(userInfo, (created) => {
                        if(created) successCreateAdminUser++;
                        else failedCreateAdminUserID.push(adminID);
                    })
                }
                else failedUploadAdmin.push(admin);
                scannedAdmin++;
                if(scannedAdmin == adminList.length) {
                    err.failedUploadAdmin = failedUploadAdmin;
                    err.failedCreateAdminUserID = failedCreateAdminUserID;
                    res.successUploadAdmin = successUploadAdmin;
                    res.successCreateAdminUser = successCreateAdminUser;
                    callback(err, res);
                }
            });
        });
    } catch(err) {
        throw err;
    }
}

function updateAdminList(updateAdminList, callback) {
    try {
        var err = {}, res = {};
        var scannedAdmin = 0, successUpdate = 0, notAffectedAdmin = [], notChangedAdmin = [];
        updateAdminList.forEach(admin => {
            var updateElement = "";
            var adminID;
            Object.keys(admin).forEach(key => {
                if(key == "adminID") {
                    adminID = admin[key];
                } else {
                    updateElement += key + "='" + String(admin[key]) + "', ";
                }
            })
            updateElement = updateElement.substring(0, updateElement.length - 2);
            DBinterfaces.updateAdmin(updateElement, adminID, (updateError, affectedRows, changedRows) => {
                
                if(updateError) {
                    console.log("Update Admin list failed at admin: " + String(admin));
                }
                else if(affectedRows == 0) {
                    notAffectedAdmin.push(admin);
                }
                else if(changedRows == 0) {
                    notChangedAdmin.push(admin);
                }
                else successUpdate++;

                scannedAdmin++;
                if(scannedAdmin == updateAdminList.length) {
                    err.notAffectedAdmin = notAffectedAdmin;
                    err.notChangedAdmin = notChangedAdmin;
                    res.successUpdate = successUpdate;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function deleteAdminList(deleteAdminIDList, callback) {
    try {
        var err = {}, res = {};
        var scannedAdmin = 0, successDelete = 0, notAffectedAdmin = [], successDeletedAdminUser = 0, failedDeletedAdminUserID = [];
        deleteAdminIDList.forEach(adminID => {
            DBinterfaces.deleteAdmin(adminID, (deleteError, affectedRows) => {
                if(deleteError) {
                    console.log("Delete admin list failed at admin: " + String(adminID));
                }
                else if(affectedRows == 1) {
                    successDelete++;
                    DBinterfaces.deleteUser(adminID, (deleteUserError, affectedUserRows) => {
                        if(deleteUserError) {
                            console.log("Delete admin user list failed at admin: " + String(adminID));
                        }
                        else if(affectedUserRows == 1) {
                            successDeletedAdminUser++;
                        }
                        else failedDeletedAdminUserID.push(adminID);
                    })
                }
                else notAffectedAdmin.push(adminID);
                scannedAdmin++;
                if(scannedAdmin == deleteAdminIDList.length) {
                    err.notAffectedAdmin = notAffectedAdmin;
                    err.failedDeletedAdminUserID = failedDeletedAdminUserID;
                    res.successDelete = successDelete;
                    res.successDeletedAdminUser = successDeletedAdminUser;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

module.exports = {
    showSpecificAdmin, listAdminsWithCondition, listAdmins, insertAdmin, updateAdmin, deleteAdmin,
    uploadAdminList, updateAdminList, deleteAdminList
}