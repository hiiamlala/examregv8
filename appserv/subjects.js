const DBinterfaces = require("./DBinterfaces")
const rooms = require("./rooms");
const classes = require("./classes");
const classLists = require("./classList");

function showSpecificSubject(ID, callback) {
    DBinterfaces.showSpecificSubject(ID, callback);
}

function listSubjectsWithCondition(condition, callback) {
    DBinterfaces.listSubjectsWithCondition(condition, callback);
}

function listSubjects(callback) {
    DBinterfaces.listSubjects(callback);
}

function insertSubject(subjectInfo, callback) {
    var successUploadSubject = false;
    var columnsString = "";
    var valuesString = "";
    Object.keys(subjectInfo).forEach(key => {
        columnsString += key + ", ";
        valuesString += '"' + subjectInfo[key] + '", ';
    });
    columnsString = columnsString.substring(0, columnsString.length - 2);
    valuesString = valuesString.substring(0, valuesString.length - 2);
    DBinterfaces.insertSubject(columnsString, valuesString, (uploadSuccessfully) => {
        if(uploadSuccessfully) {
            successUploadSubject = true;
            callback(successUploadSubject);
        }
    });
}

function updateSubject(updateInfo, subjectID, callback) {
    var updateString = "";
    Object.keys(updateInfo).forEach((updateElementKey, updateElementKeyIndex, updateElementKeyArray) => {
        if(updateElementKeyIndex == updateElementKeyArray.length - 1) {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "'";
            DBinterfaces.updateSubject(updateString, subjectID, callback);
        } else {
            updateString += updateElementKey + "='" + updateInfo[updateElementKey] + "',";
        }
    })
}

function deleteSubject(subjectID, callback) {
    DBinterfaces.deleteSubject(subjectID, callback);
}

function numberOfSlotsForSubjectID(subjectID, callback) {
    var resultNumberOfSlots = 0;
    DBinterfaces.listExamInfosWithCondition("subjectID='" + String(subjectID) + "'", (examInfoList) => {
        examInfoList.forEach((examInfo, examInfoIndex, examInfoArray) => {
            rooms.showSpecificRoom(examInfo.roomID, (roomInfo) => {
                resultNumberOfSlots += roomInfo.slots;
                if(examInfoIndex === examInfoArray.length - 1) {
                    callback(resultNumberOfSlots);
                }
            })
        })
    })
}

function listSubjectsRegisteredByStudentID(studentID, callback) {
    var resultSubjectList = [];
    classLists.listStudentsInClassesWithCondition("studentID='" + String(studentID) + "'", (resultStudentInClassList) => {
        resultStudentInClassList.forEach((studentInClass, studentInClassIndex, studentInClassArray) => {
            classes.showSpecificClass(studentInClass.classID, (classInfo) => {
                showSpecificSubject(classInfo.subjectID, (subjectInfo) => {
                    resultSubjectList.push(subjectInfo);
                    if(studentInClassIndex === studentInClassArray.length - 1) {
                        callback(resultSubjectList);
                    }
                })
            })
        })
    })
}

function uploadSubjectList(subjectList, callback) {
    try {
        var err = {}, res = {};
        var scannedSubject = 0, successUploadSubject = 0, failedUploadSubject = [];
        subjectList.forEach(subject => {
            var columnsString = "";
            var valuesString = "";
            Object.keys(subject).forEach(key => {
                columnsString += key + ", ";
                valuesString += '"' + subject[key] + '", ';
            });
            columnsString = columnsString.substring(0, columnsString.length - 2);
            valuesString = valuesString.substring(0, valuesString.length - 2);
            DBinterfaces.insertSubject(columnsString, valuesString, (uploadSuccessfully) => {
                if(uploadSuccessfully) successUploadSubject++;
                else failedUploadSubject.push(subject);
                scannedSubject++;
                if(scannedSubject == subjectList.length){
                    err.failedUploadSubject = failedUploadSubject;
                    res.successUploadSubject = successUploadSubject;
                    callback(err, res);
                }
            });
        });
    } catch(err) {
        throw err;
    }
}

function updateSubjectList(updateSubjectList, callback) {
    try {
        var err = {}, res = {};
        var scannedSubject = 0, successUpdate = 0, notAffectedSubject = [], notChangedSubject = [];
        updateSubjectList.forEach(subject => {
            var updateElement = "";
            var subjectID;
            Object.keys(subject).forEach(key => {
                if(key == "subjectID") {
                    subjectID = subject[key];
                } else {
                    updateElement += key + "='" + String(subject[key]) + "', ";
                }
            })
            updateElement = updateElement.substring(0, updateElement.length - 2);
            DBinterfaces.updateSubject(updateElement, subjectID, (updateError, affectedRows, changedRows) => {

                if(updateError) {
                    console.log("Update Subject list failed at subject: " + String(subject));
                }
                else if(affectedRows == 0) {
                    notAffectedSubject.push(subject);
                }
                else if(changedRows == 0) {
                    notChangedSubject.push(subject);
                }
                else successUpdate++;
                
                scannedSubject++;
                if(scannedSubject == updateSubjectList.length) {
                    err.notAffectedSubject = notAffectedSubject;
                    err.notChangedSubject = notChangedSubject;
                    res.successUpdate = successUpdate;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

function deleteSubjectList(deleteSubjectIDList, callback) {
    try {
        var err = {}, res = {};
        var scannedSubject = 0, successDelete = 0, notAffectedSubject = [];
        deleteSubjectIDList.forEach(subjectID => {
            DBinterfaces.deleteSubject(subjectID, (deleteError, affectedRows) => {
                if(deleteError) {
                    console.log("Delete Subject list failed at subject: " + String(subjectID));
                }
                else if(affectedRows == 0) {
                    notAffectedSubject.push(subjectID);
                }
                else successDelete++;
                
                scannedSubject++;
                if(scannedSubject == deleteSubjectIDList.length) {
                    err.notAffectedSubject = notAffectedSubject;
                    res.successDelete = successDelete;
                    callback(err, res);
                }
            })
        })
    } catch(err) {
        throw err;
    }
}

module.exports = {
    showSpecificSubject, listSubjectsWithCondition, listSubjects, insertSubject, updateSubject, deleteSubject,
    uploadSubjectList, updateSubjectList, deleteSubjectList,
    numberOfSlotsForSubjectID, listSubjectsRegisteredByStudentID
}