const mysql = require("mysql");

var connection;
//DB connection part
// var connection = mysql.createConnection({
//     host     : 'examreg.ceysu41lzk39.ap-southeast-1.rds.amazonaws.com',
//     user     : 'root',
//     password : '12345678',
//     database : 'ExamReg'
// });

initDBConnect();

function initDBConnect() {
    try {
        connection = mysql.createConnection({
            host: 'examreg.ceysu41lzk39.ap-southeast-1.rds.amazonaws.com',
            user: 'root',
            password: '12345678',
            database: 'ExamReg'
        });
        connection.connect((err) => {
            if (!err) {
                console.log("DB connect successfully");
            } else {
                initDBConnect();
                console.log("Fail to connect to DB");
            }
        });

        connection.on("error", (err) => {
            console.log("DB lost connection...");
            connection.end();
            initDBConnect();
            if (err.code == "read ECONNRESET" || err.code == "PROTOCOL_CONNECTION_LOST") {
            }
        })

        //DB interface functions part
        //Show commom info
        function showDBs(callback) {
            connection.query('SHOW DATABASES', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    var res = [];
                    result.forEach(element => {
                        res.push(element.Database);
                    });
                    callback(res);
                }
            });
        }

        function showTBs(callback) {
            connection.query('SHOW TABLES', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    var res = [];
                    result.forEach(element => {
                        res.push(element[Object.keys(result[0])[0]]);
                    });
                    callback(res);
                }
            })
        }


        //Users table
        function showSpecificUser(ID, callback) {
            var sql = "SELECT * FROM users WHERE userID='" + String(ID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listUsersWithCondition(condition, callback) {
            var sql = "SELECT * FROM users WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listUsers(callback) {
            connection.query('SELECT * FROM users', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertUser(columns, values, callback) {
            var sql = `INSERT INTO users (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateUser(updateElement, userID, callback) {
            var sql = `UPDATE users SET ${updateElement} WHERE userID='${userID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteUser(userID, callback) {
            var sql = `DELETE FROM users WHERE userID!='${userID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Students table
        function showSpecificStudent(ID, callback) {
            var sql = "SELECT * FROM students WHERE studentID='" + String(ID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listStudentsWithCondition(condition, callback) {
            var sql = "SELECT * FROM students WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listStudents(callback) {
            connection.query('SELECT * FROM students', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertStudent(columns, values, callback) {
            var sql = `INSERT INTO students (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateStudent(updateElement, maSV, callback) {
            var sql = `UPDATE students SET ${updateElement} WHERE studentID='${maSV}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteStudent(maSV, callback) {
            var sql = `DELETE FROM students WHERE studentID='${maSV}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Admins table
        function showSpecificAdmin(ID, callback) {
            var sql = "SELECT * FROM admins WHERE adminID='" + String(ID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listAdminsWithCondition(condition, callback) {
            var sql = "SELECT * FROM admins WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listAdmins(callback) {
            connection.query('SELECT * FROM admins', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertAdmin(columns, values, callback) {
            var sql = `INSERT INTO admins (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateAdmin(updateElement, adminID, callback) {
            var sql = `UPDATE admins SET ${updateElement} WHERE adminID='${adminID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteAdmin(adminID, callback) {
            var sql = `DELETE FROM admins WHERE adminID='${adminID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Rooms table
        function showSpecificRoom(ID, callback) {
            var sql = "SELECT * FROM rooms WHERE roomID='" + String(ID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listRoomsWithCondition(condition, callback) {
            var sql = "SELECT * FROM rooms WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listRooms(callback) {
            connection.query('SELECT * FROM rooms', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertRoom(columns, values, callback) {
            var sql = `INSERT INTO rooms (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateRoom(updateElement, roomID, callback) {
            var sql = `UPDATE rooms SET ${updateElement} WHERE roomID='${roomID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteRoom(roomID, callback) {
            var sql = `DELETE FROM rooms WHERE roomID='${roomID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Shifts table
        function showSpecificShift(ID, callback) {
            var sql = "SELECT * FROM shifts WHERE shiftID='" + String(ID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listShiftsWithCondition(condition, callback) {
            var sql = "SELECT * FROM shifts WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listShifts(callback) {
            connection.query('SELECT * FROM shifts', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertShift(columns, values, callback) {
            var sql = `INSERT INTO shifts (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateShift(updateElement, shiftID, callback) {
            var sql = `UPDATE shifts SET ${updateElement} WHERE shiftID='${shiftID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteShift(shiftID, callback) {
            var sql = `DELETE FROM shifts WHERE shiftID='${shiftID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Subjects table
        function showSpecificSubject(ID, callback) {
            var sql = "SELECT * FROM subjects WHERE subjectID='" + String(ID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listSubjectsWithCondition(condition, callback) {
            var sql = "SELECT * FROM subjects WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listSubjects(callback) {
            connection.query('SELECT * FROM subjects', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertSubject(columns, values, callback) {
            var sql = `INSERT INTO subjects (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateSubject(updateElement, subjectID, callback) {
            var sql = `UPDATE subjects SET ${updateElement} WHERE subjectID='${subjectID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteSubject(subjectID, callback) {
            var sql = `DELETE FROM subjects WHERE subjectID='${subjectID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Classes table
        function showSpecificClass(ID, callback) {
            var sql = "SELECT * FROM classes WHERE classID='" + String(ID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listClassesWithCondition(condition, callback) {
            var sql = "SELECT * FROM classes WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listClasses(callback) {
            connection.query('SELECT * FROM classes', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertClass(columns, values, callback) {
            var sql = `INSERT INTO classes (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateClass(updateElement, classID, callback) {
            var sql = `UPDATE classes SET ${updateElement} WHERE classID='${classID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteClass(classID, callback) {
            var sql = `DELETE FROM classes WHERE classID='${classID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Class_list table
        function showSpecificStudentInClass(studentID, classID, callback) {
            var sql = "SELECT * FROM class_list WHERE studentID='" + String(studentID) + "' AND classID='" + String(classID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listStudentsInClassesWithCondition(condition, callback) {
            var sql = "SELECT * FROM class_list WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listClassList(callback) {
            connection.query('SELECT * FROM class_list', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertClassList(columns, values, callback) {
            var sql = `INSERT INTO class_list (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateClassList(updateElement, classID, studentID, callback) {
            var sql = `UPDATE class_list SET ${updateElement} WHERE classID='${classID}' AND studentID='${studentID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteClassList(classID, studentID, callback) {
            var sql = `DELETE FROM class_list WHERE classID='${classID}' AND studentID='${studentID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Exam_info table
        function showSpecificExamInfo(ID, callback) {
            var sql = "SELECT * FROM exam_info WHERE examInfoID='" + String(ID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listExamInfosWithCondition(condition, callback) {
            var sql = "SELECT * FROM exam_info WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listExamInfo(callback) {
            connection.query('SELECT * FROM exam_info', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertExamInfo(columns, values, callback) {
            var sql = `INSERT INTO exam_info (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateExamInfo(updateElement, examInfoID, callback) {
            var sql = `UPDATE exam_info SET ${updateElement} WHERE examInfoID='${examInfoID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteExamInfo(examInfoID, callback) {
            var sql = `DELETE FROM exam_info WHERE examInfoID='${examInfoID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }


        //Exam_list table
        function showSpecificStudentInExam(studentID, examInfoID, callback) {
            var sql = "SELECT * FROM exam_list WHERE studentID='" + String(studentID) + "' AND examInfoID='" + String(examInfoID) + "'";
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result[0]);
                }
            })
        }

        function listStudentsInExamsWithCondition(condition, callback) {
            var sql = "SELECT * FROM exam_list WHERE " + String(condition);
            connection.query(sql, (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function listExamList(callback) {
            connection.query('SELECT * FROM exam_list', (err, result, fields) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(result);
                }
            })
        }

        function insertExamList(columns, values, callback) {
            var sql = `INSERT INTO exam_list (${columns}) VALUES (${values})`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(false);
                } else {
                    callback(true);
                }
            });
        }

        function updateExamList(updateElement, examInfoID, studentID, callback) {
            var sql = `UPDATE exam_list SET ${updateElement} WHERE examInfoID='${examInfoID}' AND studentID='${studentID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows, result.changedRows);
                }
            });
        }

        function deleteExamList(examInfoID, studentID, callback) {
            var sql = `DELETE FROM exam_list WHERE examInfoID='${examInfoID}' AND studentID='${studentID}'`;
            connection.query(sql, (err, result) => {
                if (err) {
                    console.log(err.sqlMessage);
                    console.log("SQL query: " + err.sql);
                    initDBConnect();
                    callback(true);
                } else {
                    callback(false, result.affectedRows);
                }
            });
        }

        //DB interface function export part
        module.exports = {
            showDBs, showTBs,
            showSpecificUser, listUsersWithCondition, listUsers, insertUser, updateUser, deleteUser,
            showSpecificStudent, listStudentsWithCondition, listStudents, insertStudent, updateStudent, deleteStudent,
            showSpecificAdmin, listAdminsWithCondition, listAdmins, insertAdmin, updateAdmin, deleteAdmin,
            showSpecificRoom, listRoomsWithCondition, listRooms, insertRoom, updateRoom, deleteRoom,
            showSpecificShift, listShiftsWithCondition, listShifts, insertShift, updateShift, deleteShift,
            showSpecificSubject, listSubjectsWithCondition, listSubjects, insertSubject, updateSubject, deleteSubject,
            showSpecificClass, listClassesWithCondition, listClasses, insertClass, updateClass, deleteClass,
            showSpecificStudentInClass, listStudentsInClassesWithCondition, listClassList, insertClassList, updateClassList, deleteClassList,
            showSpecificExamInfo, listExamInfosWithCondition, listExamInfo, insertExamInfo, updateExamInfo, deleteExamInfo,
            showSpecificStudentInExam, listStudentsInExamsWithCondition, listExamList, insertExamList, updateExamList, deleteExamList
        }
    }
    catch (er) {
        console.log(new Date().toISOString() + " | " + er);
        initDBConnect();
    }
}
